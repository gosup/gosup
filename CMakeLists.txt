# set minimum cmake version
cmake_minimum_required(VERSION 3.8 FATAL_ERROR)

# project name and language
project(GOS C CXX)
project(GOS VERSION 1.0.0)

configure_file(GOSConfig.h.in src/GOSConfig.h)


#set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# Add compiler flags
if(CMAKE_COMPILER_IS_GNUCC)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fpermissive -std=c++20 -pthread") #
endif()

# add tinyxml
set(TINY_DIR "${CMAKE_SOURCE_DIR}/libraries/tinyxml2")
message(STATUS "Building TinyXML2 parser ...")
include_directories("${TINY_DIR}/src")
add_subdirectory(${TINY_DIR})

# add pn parser
set(PARSER_DIR "${CMAKE_SOURCE_DIR}/libraries/pn-parser")
message(STATUS "Building Petri Net parser ...")
include_directories("${PARSER_DIR}/src")
add_subdirectory(${PARSER_DIR})

# add buddy
set(BUDDY_DIR "${CMAKE_SOURCE_DIR}/libraries/BuDDy")
message(STATUS "Building BuDDy library...")
include_directories("${BUDDY_DIR}/src")
add_subdirectory(${BUDDY_DIR})



# add source folder
include_directories(src)
add_subdirectory(src)
