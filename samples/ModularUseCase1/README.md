This is a modular example to test our modular GOSUP. It comprises 4 modules, each accompanied by essential files:
1. A PNML file : an XML file that decribes the Petri Net of the module.

2. A .obs file : describing the observable events of this system. To give clarity, we break down its structure:  
- The first two lines of this file describe the observation of the supervisor : $\Sigma_m$
- The following two lines describe the observation of the attacker : $\Sigma_a$
- The last two lines describe the controllable events of the supervisor : $\Sigma_c$
In general each set of events is described in two lines: the first one gives the number of events to consider, the last one gives the list of these events; seperated with a space.

3. A .sec file to describe the secret states of the system. 
This modular example is instrumental in testing the functionality and adaptability of our modular GOSUP. Feel free to explore and adapt it for your specific use cases.