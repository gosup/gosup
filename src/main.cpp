#include <iostream>
#include <string>
#include "bdd.h"
#include "NewNet.h"
#include "opacity/WeakSupervisor.h"
#include "../libraries/CLI11/CLI11.hpp"
#include "opacity/OS_Opacity_Manager.h"

using namespace std;

constexpr uint8_t cMAX = 100;

/***********************************************/
int main(int argc, char **argv) {
    CLI::App app{"GoSup : General framework Opacity SUPervision Opacity tool"};
    cout << "*******************************************************************" << endl;
    cout << "*******************************************************************" << endl;
    cout << "*     GoSup : General framework Opacity SUPervision Opacity tool  *" << endl;
    cout << "*******************************************************************" << endl;
    cout << "*******************************************************************\n" << endl;


    vector<string> lModules, lObsTransFile, lSecretStatesFile;
   auto modelOption = app.add_option<vector<string>>("--model", lModules, "XML model files; set several files to perform modular supervision")
            ->type_name("Path")
            ->required()
            ->check(CLI::ExistingFile);

    app.add_option<vector<string>>("--obs", lObsTransFile, "File(s) specifying observable transitions resp. for supervisor and attacker, and controllable transitions")
            ->type_name("Path")
            ->required()
            ->check(CLI::ExistingFile);
    app.add_option("--sec", lSecretStatesFile, "File(s) specifying secret states")
            ->type_name("Path")
            ->required()
            ->check(CLI::ExistingFile);

    CLI11_PARSE(app, argc, argv);

    // lModules ; contains list of models
    char opacityChoice[cMAX]{""};
    cout << "Performing modular opacity reinforcement....\n";
    cout << "#modules: " << lModules.size() << '\n';
    std::vector<NewNet *> lNets;
    bdd_gbc_hook(nullptr);

    bdd_init(1000000, 10000);
    WeakSupervisor wS;
    for (size_t i{0}; i < lModules.size(); ++i) {
        cout << "\n=====> Processing module #" << i << "..." << endl;
        lNets.push_back(new NewNet(lModules[i].c_str(), lObsTransFile[i].c_str(), lSecretStatesFile[i].c_str(), opacityChoice));
        RdPBDD DR(*lNets[i], 1, false);
        auto ptrOpacityManager {std::make_shared<OS_Opacity_Manager>(&DR, lNets[i]->getMObsAttTrans(), lNets[i]->getMObsSupTrans(), lModules[i].substr(0, lModules[i].find_last_of('.')))};
       // ptrOpacityManager->generateDOT();
        ptrOpacityManager->computeAndOpacify_Hyper_SOG();
        //ptrOpacityManager->generateDOT();
        //wS.add(ptrOpacityManager);
    }
    if (lModules.size()>1) {
        //std::cout << "   ****** Weak supervision : keep supervisor of module "<<i_module<<"\n";
    }
    for (const auto &net: lNets) {
        delete net;
    }

    return 0;
}

