/* -*- C++ -*- */
#include <string.h>
#include <iostream>
#include <set>
#include <vector>
#include <stack>
#include <utility>
#include "misc.h"
#include <algorithm>
#include "bvec.h"
#include "RdPBDD.h"

using namespace std;
int NbIt;
int itext, itint;
int MaxIntBdd;
bdd *TabMeta;
int nbmetastate;

//Qunatified opacity variables
vector<double> PAggs;

bool QOne = false;

const vector<class Place> *vplaces = nullptr;

void my_error_handler(int errcode) {
    if (errcode == BDD_RANGE) {
        // Value out of range : increase the size of the variables...
        // but which one???
        bdd_default_errhandler(errcode);
    } else {
        bdd_default_errhandler(errcode);
    }
}

/*****************************************************************/
/*                         printhandler                          */
/*****************************************************************/
void printhandler(ostream &o, int var) {
    o << (*vplaces)[var / 2].name; //writes node name (node type is place) in file o
    if (var % 2)
        o << "_p";
}

/*****************************************************************/
/*                         class Trans                           */
/*****************************************************************/
Trans::Trans() {
//Default constructor
}

Trans::Trans(const bdd &v, bddPair *p, const bdd &r, const bdd &pr,
             const bdd &pre, const bdd &post) :
        var(v), pair(p), rel(r), prerel(pr), Precond(pre), Postcond(post) {
    //cout << "Trans" << endl;
}

Trans::Trans(const Trans &src) {
    this->var = src.var;
    this->pair = src.pair;
    this->rel = src.rel;
    this->prerel = src.prerel;
    this->Precond = src.Precond;
    this->Postcond = src.Postcond;
//copy constructor
}

bdd Trans::getRel() {
    return this->rel;
}

bdd Trans::getVar() {
    return this->var;
}


void Trans::setRel(bdd a) {
    this->rel = a;
}

void Trans::setVar(bdd a) {
    this->var = a;
}

Trans &Trans::operator=(Trans &t) {
    if (&t != this) {
        this->pair = t.pair;
        this->rel = t.rel;
        this->prerel = t.prerel;
        this->Precond = t.Precond;
        this->Postcond = t.Postcond;
    }
    return (*this);
}

/*
 * Step forward
 */
bdd Trans::operator()(const bdd &n) const {
    bdd res;
    //cout << "Franchissement avant" << endl;
    //cout << "(n, rel, var) " << n<<"___"<< rel<<"___"<< var;
    res = bdd_relprod(n, rel, var);// 1.produit relationel entre n et rel

    res = bdd_replace(res, pair); // 2.remplacer avec le resultat du produit
    return res;
}

/*
 * Step backward
 */
bdd Trans::operator[](const bdd &n) const {
    //cout << "Franchissement arrière" << endl;
    bdd res = bdd_relprod(n, prerel, var);    // produit
    // var, prerel --> bdd
    res = bdd_replace(res, pair);                                 // remplacement
    //cout<<"res: "<<res<<endl;
    return res;
}


/*****************************************************************/
/*                         class RdPBDD                          */
/*****************************************************************/

RdPBDD::RdPBDD(const NewNet &modelNet, int BOUND, bool init) : mPNet(&modelNet) { //BOUND = b?? marquage maximal??

    int nbPlaces = modelNet.places.size(), i, domain;
    vector<Place>::const_iterator p;
    set<int>::iterator myIterator;
    set<int>::iterator It;

    //cout<<"Bound is: "<<BOUND<<endl;//just checking if bound is b from main.cpp

    // bvec the data structure that contains booelan vector

    bvec *v = new bvec[nbPlaces];
    bvec *vp = new bvec[nbPlaces];
    bvec *prec = new bvec[nbPlaces];
    bvec *postc = new bvec[nbPlaces];
    int *idv = new int[nbPlaces];
    int *idvp = new int[nbPlaces];
    int *nbbit = new int[nbPlaces];

    // each variable takes its values from the values of the net modelNet
    mlTransitions = modelNet.transitions;
    mObservables = modelNet.mObservable;
    mNonObservables = modelNet.mNonObservable;
    mControllable = modelNet.getMControllableTrans();
    Formula_Trans = modelNet.Formula_Trans;
    mTransitionName = modelNet.transitionName;
    InterfaceTrans = modelNet.InterfaceTrans;
    mPlaceName = modelNet.placeName;

    Nb_places = modelNet.places.size();
    vplaces = &modelNet.places;


    mlSecretStates = modelNet.mlSecretStates;

    mSecretBdd = bdd_true();

    //part commented deleted by me

    // cout << "#Places : " << Nb_places << endl; //print number of places
    // cout << "#Transitions: " << modelNet.transitions.size() << endl;
    // cout<<"Last place is : "<<modelNet.places[Nb_places-1].name<<endl; //print last place
    // cout << "#Observable transitions: " << modelNet.mObservable.size() << " -> ";

    /*for (Set::const_iterator it = modelNet.mObservable.begin();
         it != modelNet.mObservable.end(); it++) {
        cout << "t" << (*it) << " ";
    }
    cout << '\n';
    cout << "#Non observable transitions: " << modelNet.mNonObservable.size() << " -> ";

    for (const auto &elt: modelNet.mNonObservable) {
        cout << "t" << elt << " ";
    }
    cout << '\n';*/

    /* place domain, place bvect, place initial marking and place name */
    for (i = 0, p = modelNet.places.begin(); p != modelNet.places.end(); i++, p++) { //for i=0 and p=firstplace
        //till p=lastplace
        if (p->hasCapacity()) {
            domain = p->capacity + 1; //capcity comes from node capcity in net.h
        } else {
            domain = BOUND + 1; // the default domain
        } //this part attaches a domain (capacity+1) for each place (numbered 0 to last)

        //	cout<<"Domain"<<i<<" is: "<<domain<<endl;//just checking domain values

        // variables are created one by one (implying contigue binary variables)
        fdd_extdomain(&domain, 1);
        //cout<<"nb created var : "<<bdd_varnum()<<endl;
        fdd_extdomain(&domain, 1);
        //cout<<"nb created var : "<<bdd_varnum()<<endl;
    }
    // bvec
    currentvar = bdd_true(); // returns true bdd


    for (i = 0; i < nbPlaces; i++) {

        nbbit[i] = fdd_varnum(2 * i);//Returns the number of BDD variables used for the finite domain
        // cout<<"nbbit["<<i<<"]= "<<nbbit[i]<<endl;
        // block
        //for each domain 2 vars? --> true and false?
        //cout<<"nb var pour "<<2*i<<" = "<<fdd_domainsize(2*i)<<endl;

        v[i] = bvec_varfdd(2 * i);        // build a boolean vector
        // cout<<"v["<<i<<"]= "<<v[i]<<endl;

        vp[i] = bvec_varfdd(2 * i + 1);
        // cout<<"vp["<<i<<"]= "<<vp[i]<<endl;
        //cout<<"nb var pour "<<2*i+1<<" = "<<fdd_domainsize(2*i+1)<<endl;
        currentvar = currentvar & fdd_ithset(2 * i); // & bitwise and.
        //	cout << "current var " << currentvar <<endl;
    }

    // initial marking
    //cout <<"initial marking" <<endl ;

    // what does this part of the code do??
    //this part we create a BDD that represents the secret

    mInitialMarking = bdd_true();

    //find value of initial state
    for (i = 0, p = modelNet.places.begin(); p != modelNet.places.end(); i++, p++) { //for i=0 and p=first place
        // as long as not p=last place
        // i++ and p++
        //add test if place is a secret and set marking accordingly??
        //cout<<"initial marking: "<< fdd_ithvar(2*i,p->marking)<<endl;
        mInitialMarking = mInitialMarking & fdd_ithvar(2 * i, p->marking); // MO is true, we do an and(bitwise) with the
        //returned value of fdd_ithavar (true or false)
        //what is MO???
        //  cout<<"var= "<<2*i<<" val= "<<p->marking<<endl; //checking values for input of fdd_ithvar
    }
    // cout << "Initial state M0: " << mInitialMarking << endl;
    // cout << "it's path count is:" << bdd_nodecount(M0) << endl;

    mSecretBdd = bdd_false();
    for (i = 0; i < modelNet.mlSecretStates.size(); ++i) {
        bdd secretState = bdd_true();
        for (auto indice_place = 0; indice_place < modelNet.places.size(); ++indice_place) {
            if (mlSecretStates[i].find(indice_place) != mlSecretStates[i].end()) {
                secretState = secretState & fdd_ithvar(2 * indice_place, 1);
            } else
                secretState = secretState & fdd_ithvar(2 * indice_place, 0);
        }
        mSecretBdd = mSecretBdd | secretState;
    }

    fdd_strm_hook(printhandler);                  // printhandler
    for (vector<Transition>::const_iterator t = modelNet.transitions.begin();
         t != modelNet.transitions.end(); t++) { //for each transition
        int np;
        bdd rel = bdd_true(), var = bdd_true(), prerel = bdd_true();
        bdd Precond = bdd_true(), Postcond = bdd_true();
        // precond=true, postcond=true
        bddPair *p = bdd_newpair();

        for (i = 0; i < nbPlaces; i++) {
            //for each place build prec and postc
            prec[i] = bvec_con(nbbit[i],
                               0); // builds a boolean vector that represents the variable val(0) using nbbit[i]
            //cout<<"prec"<<i<<" is: "<<prec[i];
            //cout <<"builds a boolean vector that represents the variable val(0) using "<<nbbit[i]<< prec[i]<<endl;
            postc[i] = bvec_con(nbbit[i],
                                0);// builds a boolean vector that represent the variable val(0) using nbbit[i]
            //cout <<"builds a boolean  vector that represents the variable val(0) using "<<nbbit[i]<< postc[i]<<endl;
        }

        // calculer les places adjacentes a la transition t
        // et la relation rel de la transition t
        set<int> adjacentPlace;

        // arcs pre
        //cout <<"arcs pre"<< endl;
        //int insertadjpl=0;
        for (auto it = t->pre.begin();
             it != t->pre.end(); it++) {
            //	insertadjpl++;
            //cout<<"pre: "<<it->first<<" "<<it->second<<endl;
            //t is an iterator of transitions
            //cout<<"I am inside!!"<<endl;
            adjacentPlace.insert(it->first);
            //cout << it->first<< endl;
            //cout<<it->second<<endl;
            prec[it->first] = prec[it->first]
                              + bvec_con(nbbit[it->first], it->second);
            //cout<<prec[it->first]<<endl;
            // prec(firstvalueofiterator)=+bvec_con[nbbit[1],2nd value of it]
            //bvec_con --> boolean representation of integer value
            //exp 1-->F
            //	cout <<"it->second"<< it->second<< endl;
            //cout <<"prec[it->first]"<< prec[it->first]<< endl;
        }
        //cout<<"insertadjpl: "<<insertadjpl<<endl;
        // arcs post
        //cout <<"arcs post "<< endl;
        for (vector<pair<int, int> >::const_iterator it = t->post.begin();
             it != t->post.end(); it++) {
            adjacentPlace.insert(it->first);
            postc[it->first] = postc[it->first]
                               + bvec_con(nbbit[it->first], it->second);
            //cout <<"arcs post"<< postc[it->first]<<endl;
        }
        // arcs pre automodifiants
        for (auto it = t->preAuto.begin();
             it != t->preAuto.end(); it++) {
            adjacentPlace.insert(it->first);
            prec[it->first] = prec[it->first] + v[it->second];
            //cout <<"arcs pre automodifiants"<< prec[it->first] << endl;
        }
        // arcs post automodifiants
        for (auto it = t->postAuto.begin();
             it != t->postAuto.end(); it++) {
            adjacentPlace.insert(it->first);
            postc[it->first] = postc[it->first] + v[it->second];
            //cout <<"arcs post automodifiants"<< postc[it->first] << endl;
        }
        // arcs reset
        for (auto it = t->reset.begin();
             it != t->reset.end(); it++) {
            adjacentPlace.insert(*it);
            prec[*it] = prec[*it] + v[*it];
        }

        np = 0;
        for (set<int>::const_iterator it = adjacentPlace.begin();
             it != adjacentPlace.end(); it++) {
            idv[np] = 2 * (*it);
            idvp[np] = 2 * (*it) + 1;
            var = var & fdd_ithset(2 * (*it));
            //Image
            // precondition
            rel = rel & (v[*it] >= prec[*it]);
            Precond = Precond & (v[*it] >= prec[*it]);
            // postcondition
            rel = rel & (vp[*it] == (v[*it] - prec[*it] + postc[*it]));
            // Pre image __________
            // precondition
            prerel = prerel & (v[*it] >= postc[*it]);
            // postcondition
            Postcond = Postcond & (v[*it] >= postc[*it]);
            prerel = prerel & (vp[*it] == (v[*it] - postc[*it] + prec[*it]));
            //___________________
            // capacité
            if (modelNet.places[*it].hasCapacity())
                rel = rel & (vp[*it] <= bvec_con(nbbit[*it], modelNet.places[*it].capacity));
            np++;
        }
        fdd_setpairs(p, idvp, idv, np);

        // arcs inhibitor rel

        for (vector<pair<int, int> >::const_iterator it = t->inhibitor.begin(); it != t->inhibitor.end(); it++)
            rel = rel & (v[it->first] < bvec_con(nbbit[it->first], it->second));
        Trans tt(var, p, rel, prerel, Precond, Postcond);
        //cout<<"values of tt of "<<t->name<<" are: "<<tt.var<<" "<<tt.pair->id<<" "<<tt.pair->last<<" "<<tt.rel<<" "<<tt.prerel<<" "<<tt.Precond<<" "<<tt.Postcond<<endl;

        this->relation.push_back(tt);
//		cout<<"size of vector relation "<<relation.size()<<endl;
        //cout<<"relation vector "<<relation.front().var<<" "<<relation.front().pair->id<<" "<<relation.front().pair->last<<" "<<relation.front().rel<<" "<<relation.front().prerel<<" "<<relation.front().Precond<<" "<<relation.front().Postcond<<endl;
        /*int i=0;
         for(vector<Trans>::const_iterator itr=relation.begin(); itr!=relation.end();itr++){
         cout<<"itr: "<<(relation)[i](M0)<<endl;
         i++;
         cout<<" /n";
         }*/
        //cout<<"relation of M0 and t1"<<relation[0](M0)<<endl;
    }
    //cout <<"first relation " <<relation[1].var << endl;

    delete[] v;
    delete[] vp;
    delete[] prec;
    delete[] postc;
    delete[] idv;
    delete[] idvp;
    delete[] nbbit;

}




/************************************Saturate***********************************/
/*
        Accessible_epsilon : method defined in RdPBDD; to be defined in class Model
        Accessible_epsilon : @param : bdd
        Accessible_epsilon : @return : bdd
        Accessible_epsilon : completes all the unobservable reach of bdd in parameter
        The output is a bdd that represents the aggregates of SOG
*/
bdd RdPBDD::Accessible_epsilon(bdd From) {
    //cout << "accessible epsilon0" << endl;
    bdd M1;
    bdd M2 = From;

    do {
        //cout << "accessible epsilon02" << endl;
        M1 = M2; // pour verifier si on travaille sur le meme noeud
        //cout << "accessible epsilon12" << endl;
        //	cout<<"M2 = "<<M2<<endl;
        //cout << "accessible epsilon22" << endl;
        for (const auto &i: mPNet->mNonObservable) {
            //cout << "accessible epsilon32" << endl;
            //cout << *i << endl;//correct
            //cout <<M2 << endl;//correct
            //cout<<"relation[(*i)](M2)"<<endl;
            bdd succ = relation[i](M2); //find successors of M2 using nonobservable event i
            //cout << "accessible epsilon42" << endl;
            M2 = succ | M2;
            //cout << "accessible epsilon52" << endl;
        }
        //cout << "accessible epsilon2" << endl;


        //	cout << bdd_nodecount(M2) << endl;
    } while (M1 != M2);
    //cout << "accessible epsilon5" << endl;
    //cout << endl;
    //cout<<"M2:"<<M2<<endl;
    return M2;
}


/************************************Parametrized Saturate***********************************/
/*
        Accessible_epsilon_parametrized : method defined in RdPBDD; to be defined in class Model
        Accessible_epsilon_parametrized : @param : bdd
        Accessible_epsilon_parametrized : @param : obs : set of observable transitions
        Accessible_epsilon_parametrized : @return : bdd
        Accessible_epsilon_parametrized : completes all the unobservable reach of bdd in parameter
        The output is a bdd that represents the super aggregate of Hyper SOG
*/

/*
 * @param : initial aggregate
 * @param : observable transitions
 */
OS_SuperAggregate *RdPBDD::computeSuperAggregate(const bdd &initial, const set<int> &obsTransitions) {

    auto superAggregate = new OS_SuperAggregate{};
    auto cInitial = new Aggregate(superAggregate);
    cInitial->mBddState = initial;
    superAggregate->setInitialAggregate(cInitial);
    superAggregate->insert(cInitial);
    stack<Aggregate *> localStack;
    localStack.push(cInitial);
    do {
        auto source = localStack.top();
        localStack.pop();
        for (const auto &i: obsTransitions) {

            bdd succ = get_successor(source->mBddState, i) ;
            if (succ == bddfalse) continue;
            bdd completeSucc = Accessible_epsilon(succ);
            if (Aggregate *a = superAggregate->find(completeSucc); a == nullptr) {
                auto dest = new Aggregate(superAggregate);
                dest->mBddState = completeSucc;
                superAggregate->insert(dest);
                source->Successors.emplace_back(std::pair(dest, i));
                dest->Predecessors.emplace_back(std::pair(source, i));
                localStack.push(dest);
            } else {
                source->Successors.emplace_back(a, i);
                a->Predecessors.emplace_back(source, i);
            }
        }
    } while (!localStack.empty());

    return superAggregate;
}


bdd RdPBDD::Accessible_epsilon_parametrized(bdd From, set<int> NonObs) {
    //cout << "accessible epsilon0" << endl;
    bdd M1;
    bdd M2 = From;
    int it = 0;

    do {
        //cout << "accessible epsilon02" << endl;
        M1 = M2; // pour verifier si on travaille sur le meme noeud
        //cout << "accessible epsilon12" << endl;
        //	cout<<"M2 = "<<M2<<endl;
        //cout << "accessible epsilon22" << endl;
        for (const auto &i: NonObs) {
            //cout << "accessible epsilon32" << endl;
            //cout << *i << endl;//correct
            //cout <<M2 << endl;//correct
            //cout<<"relation[(*i)](M2)"<<endl;
            bdd succ = relation[i](M2); //find successors of M2 using nonobservable event i
            //cout << "accessible epsilon42" << endl;
            M2 = succ | M2;
            //cout << "accessible epsilon52" << endl;
        }

        //	cout << bdd_nodecount(M2) << endl;
    } while (M1 != M2);
    //cout << "accessible epsilon5" << endl;
    //cout << endl;
    //cout<<"M2:"<<M2<<endl;
    return M2;
}


/************************************QSaturate***********************************/
bdd RdPBDD::QSaturate(bdd From, bool init) {
    bdd M1;
    bdd M2 = From;
    std::vector<int> vtr;
    double NT = 0, NST = 0, aggD = 0;
    int it = 0;

    //initiliaze trace count
    NT = bdd_pathcount(From);
    //cout<<"NT (initial size of input aggr)= "<<NT<<endl;

    //initialize secret traces count
    // test secret input state
    bdd test = bdd_true();
    test = M2 & mSecretBdd;
    if (test == M2) NST++;
    //cout<<"NST (initial count of secret traces in input aggr)= "<<NST<<endl;


    do {

        M1 = M2;

        for (const auto &i: mNonObservables) {

            bdd succ = relation[i](M2);


            if (succ != bdd_false()) {
                //
                if (!(std::find(vtr.begin(), vtr.end(), i) != vtr.end())) {
                    NT++;
                    //	int j= *i;
                    //	cout<<"succ par t"<<j+1<<" = "<<succ<<endl;
                    //	cout<<"updated NT="<<NT<<endl;
                    vtr.push_back(i);

                    // test secret succ
                    bdd testing;;
                    testing = succ & mSecretBdd;
                    if (testing != bdd_false()) {
                        NST++;
                        //	cout<<"updated NST= "<<NST<<endl;

                    }

                }

            }
            //test if succ is secret
            //update NT & NST
            M2 = succ | M2;
        }

        TabMeta[nbmetastate] = M2;


        int intsize = bdd_anodecount(TabMeta, nbmetastate + 1);
        if (MaxIntBdd < intsize)
            MaxIntBdd = intsize;
        it++;

    } while (M1 != M2);

/*	if(init == true){
		NT-=1;
	}*/
    cout << '\n';
    //cout<<"Aggregate's NT= "<<NT<<endl;
    //cout<<"Aggregate's NST= "<<NST<<endl;
    if (NT != 0) {
        aggD = NST / NT;
        cout << "Current Aggregate's Opacity Degree aggD= " << aggD << endl;
        cout << '\n';
        if (aggD >= 1) QOne = true;
        PAggs.push_back(aggD);
    }
    //insert found degrees to a vector, and cal overall degree
    return M2;
}
/*------------------------  StepForward()  --------------*/
//part was not comented begin
bdd RdPBDD::StepForward2(bdd From) {
    // cout<<"Debut Step Forward \n";
    bdd Res;
    for (const auto &i: mNonObservables) {
        bdd succ = relation[i](From);
        Res = Res | succ;
    }
    //cout<<"Fin Step Forward \n";
    return Res;
}

bdd RdPBDD::StepForward(bdd From) {
    //cout<<"Debut Step Forward \n";
    bdd Res = From;
    for (const auto &i: mNonObservables) {
        bdd succ = relation[i](Res);
        Res = Res | succ;
    }
    //cout<<"Fin Step Forward \n";
    return Res;
} //part was not comented end

/*--------------- verif()---------------------------------*/
//bdd RdPBDD::VerificationPost( int t)
//{
//   return relation[t].Postcond;
//   cout<< relation[t].Postcond<<endl ;
//}

/*------------------------  StepBackward()  --------------*/
bdd RdPBDD::StepBackward2(bdd From) {
    bdd Res;
    //cout<<"Ici Step Back : From.id() = "<<From.id()<<endl;
    for (vector<Trans>::const_iterator t = relation.begin();
         t != relation.end(); t++) {
        bdd succ = (*t)[From];
        Res = Res | succ;
        //  cout<<"Res.id() = "<<Res.id()<<endl;
    }
    // cout<<"Res.id() = "<<Res.id()<<endl;
    return Res;
}

bdd RdPBDD::StepBackward(bdd From) {
    bdd Res = From;
    for (vector<Trans>::const_iterator t = relation.begin();
         t != relation.end(); t++) {
        bdd succ = (*t)[Res];
        Res = Res | succ;
    }
    return Res;
}

/*---------------------------GetSuccessor()-----------*/
bdd RdPBDD::get_successor(bdd From, int t) {
    return relation[t](From);
}

/*------------------------Firable Obs()--------------*/
/*
        firable_obs : method defined in RdPBDD; to be defined in class Model
        //firable_obs : @param : bdd
        //firable_obs : @returns : set<int> : represents transitions
        //firable_obs : all firable transitions from bdd in param
        //firable_obs : attitude depends on the type of the model used
        //firable_obs : pure virtual method in abstract class model
        //firable_obs : to be redefined in RdPBDD
*/
Set RdPBDD::firable_obs(bdd State)    //input is shared nodes
{
    Set res;
    for (const auto &i: mObservables) {
        //cout<<"i :"<<*i<<endl;
        bdd succ = relation[i](State);//set of states (transitions, places) reachable by a State.
        if (succ != bddfalse)
            res.insert(i);
    }
    return res;
}

/*
 * Compute observable transitions in a Super Aggregate according a set of observable transitions
 * @param : Super Aggregate
 * @param : Observable transitions
 */
setPairClassSetint RdPBDD::firableObs(const OS_SuperAggregate &superAgg, const Set &obsTrans) {
    setPairClassSetint res;
    Set transitions;
    for (const auto &elt: superAgg.m_lAggregates) {
        bdd State = elt->mBddState;
        transitions.clear();
        for (const auto trans: obsTrans) {
            bdd succ = relation[trans](State);
            if (succ != bddfalse) transitions.insert(trans);
        }
        if (!transitions.empty()) {
            pair<Aggregate *, set<int>> p = std::make_pair(std::ref(elt), transitions);
            res.insert(p);
        }
    }

    return res;
}


/*-----------------------CanonizeR()----------------*/
bdd RdPBDD::CanonizeR(bdd s, unsigned int i) {
    bdd s1, s2;
    do {
        // cout<<"canonizeR !!"<<endl;
        //  cout<<"s: "<<s<<endl;
        itext++;
        s1 = s - bdd_nithvar(2 * i);//s- the negation of the variable 2*i(0?)
        // 	cout<<"s1: "<<s1<<endl;
        s2 = s - s1;
        // 	cout<<"s2: "<<s2<<endl;
        if ((!(s1 == bddfalse)) && (!(s2 == bddfalse))) {
            bdd front = s1;
            //cout<<"front "<<front<<endl;
            bdd reached = s1;
            //cout<<"reached: "<<reached<<endl;
            do {
                // cout<<"premiere boucle interne \n";
                itint++;
                front = StepForward(front) - reached;
                reached = reached | front;
                s2 = s2 - front;
            } while ((!(front == bddfalse)) && (!(s2 == bddfalse)));
        }
        if ((!(s1 == bddfalse)) && (!(s2 == bddfalse))) {
            bdd front = s2;
            bdd reached = s2;
            do {
                // cout<<"deuxieme boucle interne \n";
                itint++;
                front = StepForward(front) - reached;
                reached = reached | front;
                s1 = s1 - front;
            } while ((!(front == bddfalse)) && (!(s1 == bddfalse)));
        }
        s = s1 | s2;
        i++;
    } while ((i < Nb_places) && ((s1 == bddfalse) || (s2 == bddfalse)));
    if (i >= Nb_places) {
        //cout<<"____________oooooooppppppppsssssssss________\n";
        return s;
    } else {
        //  cout<<"________________p a s o o o p p p s s s ______\n";
        return (CanonizeR(s1, i) | CanonizeR(s2, i));
    }
}
/*------------------- GETpreL--------------*/
/*-----------------convert to string------------------*/
bdd RdPBDD::GetPrerel(bdd s, int t) {
    return relation[t].prerel;
}


/*-----------FrontiereNodes() pour bdd ---------*/
bdd RdPBDD::FrontiereNodes(bdd From) const {
    bdd res = bddfalse;
    for (const auto &i: mObservables)
        res = res | (From & relation[i].Precond);
    for (Set::const_iterator i = InterfaceTrans.begin();
         !(i == InterfaceTrans.end()); i++)
        res = res | (From & relation[*i].Precond);
    return res;
}

/*------------------------EmersonLey ----------------------------*/
bdd RdPBDD::EmersonLey(bdd S, bool trace) {
    cout << "ICI EMERSON LEY \n";
    double TpsInit, TpsDetect;
    double debitext, finitext;
    TpsInit = (double) (clock()) / CLOCKS_PER_SEC;
    bdd b = S;
    bdd Fair = bdd_ithvar(2 * Nb_places - 1);
    cout << "PLACE TEMOIN \n";
    //cout<<places[places.size()-1].name<<endl;
    bdd oldb;
    bdd oldd, d;
    int extit = 0;
    int init = 0;
    do {
        extit++;
        if (trace) {
            cout << "ITERATION EXTERNES NUMERO :" << extit << endl;
            debitext = (double) (clock()) / CLOCKS_PER_SEC;
            cout << "TAILLE DE B AVANT IT INTERNE : " << bdd_nodecount(b)
                 << endl;
            cout << endl << endl;
        }
        oldb = b;
        //cout<<"Fair : "<<Fair.id()<<endl;
        d = b & Fair;
        //cout<<"d : "<<d.id()<<endl;
        //init=0;
        do {
            init++;
            if (trace) {

                cout << "ITERATION INTERNES NUMERO :" << init << endl;
                cout << "HEURE : " << (double) (clock()) / CLOCKS_PER_SEC
                     << endl;
                cout << "TAILLE DE D : " << bdd_nodecount(d) << endl;
            }
            oldd = d;
            bdd inter = b & StepForward2(d);
            //cout<<"Tille de inter :"<<bdd_nodecount(inter)<<endl;
            d = d | inter;
        } while (!(oldd == d));
        if (trace)
            cout << "\nTAILLE DE D APRES ITs INTERNES : " << bdd_nodecount(d)
                 << endl;
        b = b & StepBackward2(d);
        init++;
        if (trace) {
            cout << "\n\nTAILLE DE B APRES ELEMINER LES PRED DE D : "
                 << bdd_nodecount(b) << endl;
            finitext = (double) (clock()) / CLOCKS_PER_SEC;
            cout << "DUREE DE L'ITERATION EXTERNE NUMERO " << extit << "  :  "
                 << finitext - debitext << endl;
            cout << endl
                 << "_________________________________________________\n\n";
        }
    } while (!(b == oldb));
    cout << "NOMBRE D'ITERATIONS EXTERNES -----:" << extit << endl;
    cout << "NOMBRE D'ITERATIONS INTERNES -----:" << init << endl;
    TpsDetect = ((double) (clock()) / CLOCKS_PER_SEC) - TpsInit;
    cout << "DETECTION DE CYCLE TIME  " << TpsDetect << endl;
    return b;
}
