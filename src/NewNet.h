/* -*- C++ -*- */
#ifndef NET_H
#define NET_H

#include <string>
#include <cstring>
#include <iostream>
#include <vector>
#include <set>
#include <map>
#include "RdPMonteur.hpp"

typedef set<int> Set;

class Node {
public:
    Node()=default;
    ~Node()=default;
    vector<pair<int, int> > pre, post, inhibitor, preAuto, postAuto;
    vector<int> reset;

    void addPre(int, int);

    void addPost(int, int);

    void addInhibitor(int, int);

    void addPreAuto(int, int);

    void addPostAuto(int, int);

    void addReset(int);
};

class Place : public Node {
public:
    string name;
    int marking, capacity;

    explicit Place(const string &p, int m = 0, int c = 0) : name(p), marking(m), capacity(c) {};

    ~Place()=default;

    bool isLossQueue() const { return marking == -2; }

    bool isMarker() const { return marking > 0; }

    bool isQueue() const { return marking <= -1; }

    bool hasCapacity() const { return capacity != 0; }
};

class Transition : public Node {
public:
    string name;

    Transition(const string &t) : name(t) {};
    ~Transition()=default;
};

/*-----------------------------------------------------------------*/
struct ltstr {
    bool operator()(const char *s1, const char *s2) const {
        return strcmp(s1, s2) < 0;
    }
};

typedef set<const char *, ltstr> Set_mot;
typedef vector<Transition> vec_transition_t;

class NewNet : public RdPMonteur {
private:
    bool Set_Formula_Trans(const char *file);

    void Set_Non_Observables();

    void readOpacityChoice(const char *f);

    void Set_Secrets(const char *f);

    int findSecretState(const string& state);
    set<int> mObsAttTrans;
    set<int> mObsSupTrans;
    set<int> mControllableTrans;
public:
    const set<int> &getMObsAttTrans() const;

    const set<int> &getMObsSupTrans() const;

    const set<int> &getMControllableTrans() const;
    /* Attributs */
    vector<class Place> places;
    vector<class Place> Secret;
    vector<class Transition> transitions;
    map<string, int> placeName;
    map<string, int> transitionName;
    Set mObservable;
    Set mNonObservable;
    vector<Set> mlSecretStates;
    Set InterfaceTrans;
    Set Formula_Trans;
    /* Constructors */
    NewNet()=delete;
    ~NewNet()=default;

    //net(const char *file,const char*Obs="", const char* Int="");
    NewNet(const char *f, const char *Formula_trans = "", const char *Formula_Secret = "",
           const char *opacityChoix = "");

    /* Monteur */
    bool addPlace(const string &place, int marking = 0, int capacity = 0);

    bool addQueue(const string &place, int capacity = 0);

    bool addLossQueue(const string &place, int capacity = 0);

    bool addTrans(const string &transition);

    bool addPre(const string &place, const string &transition, int valuation = 1);

    bool addPost(const string &place, const string &transition, int valuation = 1);

    bool addPreQueue(const string &place, const string &transition, int valuation = 1);

    bool addPostQueue(const string &place, const string &transition, int valuation = 1);

    bool addInhibitor(const string &place, const string &transition, int valuation = 1);

    bool addPreAuto(const string &place, const string &transition, const string &valuation);

    bool addPostAuto(const string &place, const string &transition, const string &valuation);

    bool addReset(const string &place, const string &transition);

    /* Visualisation */
    int nbPlace() const { return places.size(); };

    int nbTransition() const { return transitions.size(); };

    bool createXML(const char *f);

    bool addPP(const char *f);
};

ostream &operator<<(ostream &, const NewNet &);

#endif
