#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <map>
#include <algorithm>
#include "../libraries/tinyxml2/tinyxml2.h"
#include "NewNet.h"

#define TAILLEBUFF 16384
/***********************************************************/
/*                      class Node                         */
/***********************************************************/

void Node::addPre(int node, int valuation) {
    pair<int, int> x(node, valuation);


    pre.push_back(x);
}

void Node::addPost(int node, int valuation) {
    pair<int, int> x(node, valuation);
    post.push_back(x);
}

void Node::addInhibitor(int node, int valuation) {
    pair<int, int> x(node, valuation);
    inhibitor.push_back(x);
}

void Node::addPreAuto(int node, int valuation) {
    pair<int, int> x(node, valuation);
    preAuto.push_back(x);
}

void Node::addPostAuto(int node, int valuation) {
    pair<int, int> x(node, valuation);
    postAuto.push_back(x);
}

void Node::addReset(int node) {
    reset.push_back(node);
}

/***********************************************************/
/*                      class RdPE                         */
/***********************************************************/
NewNet::NewNet(const char *f, const char *Formula_trans, const char *Formula_Secret, const char *opacityChoix) {//Formula_trans is Obs??--yes
    // cout<<"Formula_trans:"<<Formula_trans<<endl;
    //cout<<"CREATION D'UN NOUVEAU SOUS-RESEAU \n";
    //createXML fills out trans, places in R.
    if (createXML(f)) {
        addPP(f);
        for (vector<class Place>::iterator p = places.begin(); p != places.end(); p++) {
            //cout<<"I went in pla \n";
            sort(p->pre.begin(), p->pre.end());
            sort(p->post.begin(), p->post.end());
        }
        for (vector<class Transition>::iterator p = transitions.begin(); p != transitions.end(); p++) {
            //cout<<"I went in trans \n";
            sort(p->pre.begin(), p->pre.end());
            sort(p->post.begin(), p->post.end());
        }
    } else {
        // cout<<"clearing everything \n"<<endl;//added by me
        places.clear();
        transitions.clear();
        placeName.clear();
        transitionName.clear();
    }
    // cout<<"Formula_trans length"<<strlen(Formula_trans)<<endl;
    if (strlen(Formula_trans) > 0) {
        //  cout<<"transitions de la formule non vide \n"; //comment
        Set_Formula_Trans(Formula_trans);//returns boolean
        if (strlen(Formula_Secret) > 0) //
        {
            Set_Secrets(Formula_Secret);

        }
        if (strlen(opacityChoix) > 0) {
            readOpacityChoice(opacityChoix);
        }

        //cout<<"______________66666666666666666666666______________________\n";
        set_union(InterfaceTrans.begin(), InterfaceTrans.end(), Formula_Trans.begin(), Formula_Trans.end(), inserter(mObservable, mObservable.begin()));
        Set_Non_Observables();
    } else
        for (int i{0}; i < transitions.size(); ++i)
            mObservable.insert(i);
//cout<<"FIN CREATION \n";
}
/*---------------------------------Init Set of  transitions ------------------------------*/
/*---------------------------------Set_formula_trans()------------------*/
void NewNet::readOpacityChoice(const char *f) {
    //cout<<"opacity choice reading \n"<<endl; //added by me
    ifstream fichier(f, ios::in);  // on ouvre en lecture

    if (fichier)  // si l'ouverture a fonctionné
    {

        string opacity;  // déclaration d'une chaîne qui contiendra la ligne lue
        string knumber;
        getline(fichier, opacity);  // on met dans "contenu" la ligne

        try {
            getline(fichier, knumber);

        } catch (int e) {
            cout << e;
        }
        fichier.close();
    } else {
        cerr << "cannot open file opacity choice !" << endl;
    }

}

//----------------Set_Formula_Trans---------------------
bool NewNet::Set_Formula_Trans(const char *f) {
    int pos_trans(vec_transition_t, string);

    std::ifstream infile(f);
    if (infile.fail()) {
        cout << "Error: file " << f << " doesn't exist" << endl;
        exit(1);
    }
    /*
     * Read observable transitions by supervisor
     */
    uint32_t nbTransitions;
    infile >> nbTransitions;

    for (auto i = 0; i < nbTransitions; ++i) {
        string token;
        infile >> token;
        int pos = pos_trans(transitions, token);
        if (pos == -1) {
            cout << "Specified observable transition for supervisor '" << token << "' doesnt exist!" << endl;
            exit(1);
        } else {
            mObsSupTrans.insert(pos);
            Formula_Trans.insert(pos);
        }
        //cout<<"Token is read: "<<token<<endl;
    }
    /*
      * Read transition observable by the attacker
      */
    infile >> nbTransitions;
    for (auto i = 0; i < nbTransitions; ++i) {
        string token;
        infile >> token;
        int pos = pos_trans(transitions, token);
        if (pos == -1) {
            cout << "Specified observable transition for attacker '" << token << "' doesnt exist!" << endl;
            exit(1);
        } else {
            mObsAttTrans.insert(pos);
            Formula_Trans.insert(pos);
        }
    }
    /*
     * Reading controllable transitions
     */
    infile >> nbTransitions;
    for (auto i = 0; i < nbTransitions; ++i) {
        string token;
        infile >> token;
        int pos = pos_trans(transitions, token);
        if (pos == -1) {
            cout << "Specified controllable transition  '" << token << "' doesnt exist!" << endl;
            exit(1);
        } else {
            mControllableTrans.insert(pos);
        }
    }
    return true;
}

/*---------------------------------Set_Secrets--------------------------*/
void NewNet::Set_Secrets(const char *f) {
    //cout<<"Set_Secrets \n"<<endl; //added by me
    ifstream in(f, ios_base::in);
    if (!in.is_open()) {
        cout << "Error opening file for secret states!\n";
        exit(-1);
    }
    //Read number of secret states

    size_t nbStates;
    in >> nbStates;
    for (auto i = 0; i < nbStates; i++) {
        // Read markings of places
        string token;
        Set S;
        do {
            in >> token;
            if (token != ";") {
                int pos = findSecretState(token);
                if (pos == -1) {
                    cout << "Specified secret state \"" << token << "\" not found!!\n";
                    exit(-1);

                }
                S.insert(pos);
            }
        } while (token != ";");
        mlSecretStates.emplace_back(S);
    }
    in.close();

}

/*---------------------------------Set_Non_Observables()------------------*/

void NewNet::Set_Non_Observables() {
    //cout<<"Set_Non_Observables \n"<<endl; //added by me
    mNonObservable.clear();
    for (unsigned int i = 0; i < transitions.size(); i++)
        if (mObservable.find(i) == mObservable.end()) {
            //cout<<"non observable"<<i<<endl;
            mNonObservable.insert(i);
        }
}

/*-----------------------pos-mlSecretStates()--------------------*/
int NewNet::findSecretState(const std::string &state) {

    int pos{0};
    for (const auto &elt: places) {
        if (elt.name == state) {
            return pos;
        }
        ++pos;
    }
    return -1;
}


/*-----------------------pos_trans()--------------------*/
int pos_trans(vec_transition_t T, string trans) {
    //cout<<"pos_Secret \n"<<endl; //added by me
    int pos = 0;
    //	cout<<"on cherche "<<trans<<" dans :\n";
    for (vec_transition_t::const_iterator i = T.begin(); !(i == T.end()); i++, pos++) {
        //  cout<<i->name<<"   ";
        if (i->name == trans)
            return pos;
    }
    //cout<<"Non trouve :\n";
    return -1;
}

/* ------------------------------ operator<< -------------------------------- */
ostream &operator<<(ostream &os, const Set &s) {
    //cout<<"operator \n"<<endl; //added by me
    bool b = false;

    if (!s.empty()) {
        for (auto i = s.begin(); !(i == s.end()); i++) {
            if (b)
                os << ", ";
            else
                os << "{";
            os << *i << " ";
            b = true;
        }
        os << "}";
    } else
        os << "empty set";
    return os;

}

/*----------------------------------------------------------------------*/


bool NewNet::addPlace(const string &place, int marking, int capacity) {
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end()) {
        placeName[place] = places.size();
        Place p(place, marking, capacity);
        places.push_back(p);
        return true;
    } else
        return false;
}

bool NewNet::addQueue(const string &place, int capacity) {
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end()) {
        placeName[place] = places.size();
        Place p(place, -1, capacity);
        places.push_back(p);
        return true;
    } else
        return false;
}

bool NewNet::addLossQueue(const string &place, int capacity) {
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end()) {
        placeName[place] = places.size();
        Place p(place, -2, capacity);
        places.push_back(p);
        return true;
    } else
        return false;
}

bool NewNet::addTrans(const string &trans) {
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end()) {
        transitionName[trans] = transitions.size();
        Transition t(trans);
        transitions.push_back(t);
        return true;
    } else
        return false;
}

bool NewNet::addPre(const string &place, const string &trans, int valuation) {
    //cout<<"place inputed: "<<place<<" transition inputed: "<<trans<<endl;
    int p, t;
    map<string, int>::const_iterator pi = placeName.find(place);
    // cout<<"pi 1st: "<<pi->first<<"pi 2nd: "<<pi->second<<endl;
    if (pi == placeName.end() || places[pi->second].isQueue())
        return false;
    else {
        p = pi->second;
        //  cout<<"I added pre place! \n";
    }
    map<string, int>::const_iterator ti = transitionName.find(trans);
    //cout<<"ti 1st: "<<ti->first<<"ti 2nd: "<<ti->second<<endl;
    if (ti == transitionName.end())
        return false;
    else {
        t = ti->second;
        //  cout<<"I added pre transition! \n";
    }

    transitions[t].addPre(p, valuation);
    //cout<<"about to add pre of"<<p<<" and it is: "<<valuation<<endl;
    places[p].addPost(t, valuation);
    return true;
}

bool NewNet::addPost(const string &place, const string &trans, int valuation) {
    int p, t;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end() || places[pi->second].isQueue())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    transitions[t].addPost(p, valuation);
    places[p].addPre(t, valuation);
    return true;
}

bool NewNet::addPreQueue(const string &place, const string &trans, int valuation) {
    int p, t;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end() || !places[pi->second].isQueue())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    transitions[t].addPre(p, valuation);
    places[p].addPost(t, valuation);
    return true;
}

bool NewNet::addPostQueue(const string &place, const string &trans, int valuation) {
    int p, t;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end() || !places[pi->second].isQueue())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    transitions[t].addPost(p, valuation);
    places[p].addPre(t, valuation);
    return true;
}

bool NewNet::addInhibitor(const string &place, const string &trans, int valuation) {
    int p, t;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    transitions[t].addInhibitor(p, valuation);
    places[p].addInhibitor(t, valuation);
    return true;
}

bool NewNet::addPreAuto(const string &place, const string &trans, const string &valuation) {
    int p, t, v;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end() || places[pi->second].isQueue())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    map<string, int>::const_iterator pv = placeName.find(valuation);
    if (pv == placeName.end() || places[pv->second].isQueue())
        return false;
    else
        v = pv->second;
    transitions[t].addPreAuto(p, v);
    places[p].addPostAuto(t, v);
    return true;
}

bool NewNet::addPostAuto(const string &place, const string &trans, const string &valuation) {
    int p, t, v;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end() || places[pi->second].isQueue())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    map<string, int>::const_iterator pv = placeName.find(valuation);
    if (pv == placeName.end() || places[pi->second].isQueue())
        return false;
    else
        v = pv->second;
    transitions[t].addPostAuto(p, v);
    places[p].addPreAuto(t, v);
    return true;
}

bool NewNet::addReset(const string &place, const string &trans) {
    int p, t;
    map<string, int>::const_iterator pi = placeName.find(place);
    if (pi == placeName.end())
        return false;
    else
        p = pi->second;
    map<string, int>::const_iterator ti = transitionName.find(trans);
    if (ti == transitionName.end())
        return false;
    else
        t = ti->second;
    transitions[t].addReset(p);
    places[p].addReset(t);
    return true;
}

/* Visualisation */
ostream &operator<<(ostream &os, const NewNet &R) {

    os << "affichage nombre de places et de transitions" << endl;
    os << "***************************" << endl;
    os << "Nombre de places     :" << R.nbPlace() << endl;
    os << "Nombre de transitions:" << R.nbTransition() << endl;

/* affichage de la liste des places */
    os << "********** places **********" << endl;
    int i = 0;
    for (auto p = R.places.begin(); p != R.places.end(); p++, i++) {
        if (p->isQueue()) {
            os << "queue hahaha net.cpp " << setw(4) << i << ":" << p->name << ", cp(" << p->capacity << ")";
            if (p->isLossQueue())
                cout << " loss";
            cout << endl;
        } else
            os << "place " << setw(4) << i << ":" << p->name << ":" << p->marking << " <..>, cp(" << p->capacity << ")" << endl;
    }
    os << "********** transitions  de la formule  **********" << endl;
    for (auto h = R.Formula_Trans.begin(); !(h == R.Formula_Trans.end()); h++)
        cout << R.transitions[*h].name << endl;
    os << "********** transitions  de l'interface  **********" << endl;
    for (auto h = R.InterfaceTrans.begin(); !(h == R.InterfaceTrans.end()); h++)
        cout << R.transitions[*h].name << endl;
    os << "Nombre de transitions observable:" << R.mObservable.size() << endl;
    os << "********** transitions observables **********" << endl;
    for (auto h = R.mObservable.begin(); !(h == R.mObservable.end()); h++)
        cout << R.transitions[*h].name << endl;
    os << "Nombre de transitions non observees:" << R.mNonObservable.size() << endl;
    os << "********** transitions  non observees **********" << endl;
    for (auto h = R.mNonObservable.begin(); !(h == R.mNonObservable.end()); h++)
        cout << R.transitions[*h].name << endl;
    i = 0;
    os << "Nombre global de transitions :" << R.nbTransition() << endl;
    os << "********** transitions  **********" << endl;
    for (auto t = R.transitions.begin(); t != R.transitions.end(); t++, i++) {
        os << setw(4) << i << ":" << t->name << endl;

//comented part 2 I deleted
    }
    return (os);
}


static RdPMonteur *R;

bool NewNet::createXML(const char *f) {
    /*---------------------------------------------------------------------*/
    string line;
    // open input file
    ifstream in(f);
    if (!in.is_open()) {
        cout << "Input file failed to open\n";
        return true;
    }
    // now open temp output file
    ofstream out("outfile.txt");
    // loop to read/write the file. Note that you need to add code here to check
    // if you want to write the line
    while (getline(in, line)) {
        if (line.find("toolspecific") != string::npos) {

        } else if (line.find("name") != string::npos) {

        } else if (line.find("text") != string::npos) {

        } else if (line.find("graphics") != string::npos) {

        } else if (line.find("position") != string::npos) {

        } else if (line.find("offset") != string::npos) {

        } else {
            out << line << "\n";
        }
    }
    in.close();
    out.close();
    // delete the original file
    remove(f);
    // rename old to new
    rename("outfile.txt", f);
    /*--------------------------------------------------------*/
    tinyxml2::XMLDocument doc;
    if (doc.LoadFile(f) != 0) {
        std::cout << "erreur lors du chargement" << std::endl;
        std::cout << "error #" << doc.ErrorID() << " : " << doc.ErrorStr()
                  << std::endl;
        return false;
    }
    R = this;
    tinyxml2::XMLHandle hdl(&doc);
    tinyxml2::XMLElement *elem = hdl.FirstChildElement().FirstChildElement().ToElement();
    tinyxml2::XMLElement *eleme =
            hdl.FirstChildElement().FirstChildElement().FirstChildElement().ToElement();
    tinyxml2::XMLElement *place_taran_arc =
            hdl.FirstChildElement().FirstChildElement().FirstChildElement().FirstChildElement().ToElement();
    tinyxml2::XMLElement *grapikMarkin =
            hdl.FirstChildElement().FirstChildElement().FirstChildElement().FirstChildElement().FirstChildElement().ToElement();
//   	if(!elem)
//    {
//        std::cout << "le noeud à atteindre n'existe pas" << std::endl;
//        return false;
//    }
    // while (elem)
    //{
    // if(elem->ValueTStr()=="page")
//{
//   while( eleme)
//   {
//	   if(eleme->ValueTStr()=="toolspecific")
//	   {std::cout<<"specific"<<endl;}
//	   else if(eleme->ValueTStr()=="name")
//	   {std::cout<<"name"<<endl;}
//	   else if(eleme->ValueTStr()=="page")
//	   {std::cout<<":)"<< std::endl;
//	   std::cout <<place_taran_arc->Attribute("tool")<<std::endl;
    while (place_taran_arc) {

        std::string a = place_taran_arc->Attribute("id");
        //  std::cout<< "parcout"<<parcourt->Attribute("id")<<std::endl;

        if (string(place_taran_arc->Value()) == "place") {
            if (place_taran_arc->FirstChildElement()) {    //	std::cout<<"palce mar"<< endl;
                //int mark = atoi (place_taran_arc->FirstChildElement()->FirstChildElement()->GetText());
                //std::cout <<place_taran_arc->Attribute("id") << std::endl;
                R->addPlace(place_taran_arc->Attribute("id"), 1, 0);
                //std::cout <<"nbr de marqage"<<place_taran_arc->FirstChildElement()->FirstChildElement()->GetText()<< std::endl;
            } else {
                //std::cout << "parcout mech mark"<<place_taran_arc->Attribute("id") << std::endl;
                R->addPlace(place_taran_arc->Attribute("id"), 0, 0);
            }
        } else if (string(place_taran_arc->Value()) == "transition") {
            R->addTrans(place_taran_arc->Attribute("id"));
        } else if (string(place_taran_arc->Value()) == "arc") {
            //std::cout<<" les arcs sont"<<std::endl;
            //std::cout << place_taran_arc->Attribute("id") << std::endl;
        }
        place_taran_arc = place_taran_arc->NextSiblingElement();

    }

    return true;
}

bool NewNet::addPP(const char *f) {
    tinyxml2::XMLDocument doc;
    if (doc.LoadFile(f) != 0) {
        std::cout << "error #" << doc.ErrorID() << " : " << doc.ErrorStr()
                  << std::endl;
        return false;
    }
    R = this;
    tinyxml2::XMLHandle hdl(&doc);

    for (tinyxml2::XMLElement *place_taran_arc =
            hdl.FirstChildElement().FirstChildElement().FirstChildElement().FirstChildElement().ToElement(); place_taran_arc != nullptr; place_taran_arc =
                                                                                                                                                 place_taran_arc->NextSiblingElement()) {
        //cout << "first value: "<<place_taran_arc->Attribute("id") << endl;
        if (string(place_taran_arc->Value()) == "place") {
            std::string a = place_taran_arc->Attribute("id");
            for (tinyxml2::XMLElement *parcourt =
                    hdl.FirstChildElement().FirstChildElement().FirstChildElement().FirstChildElement().ToElement();
                 parcourt != nullptr; parcourt =
                                              parcourt->NextSiblingElement()) { //loopindex++;
                if (string(parcourt->Value()) == "arc") {
                    //std::cout<<"on est mainetemant dans le boucle while  "<< parcourt->Attribute("id")<<std::endl;// comment
                    if (parcourt->Attribute("source") == a) {
                        std::string Pre = parcourt->Attribute("target");
                        R->addPre(a, Pre, 1);
                    } else if (parcourt->Attribute("target") == a) {
                        std::string Post = parcourt->Attribute("source");
                        R->addPost(a, Post, 1);
                    }
                } else {                        //std::cout <<":p"<<std::endl;}
                }
            }
            //	place_taran_arc = place_taran_arc->NextSiblingElement();
            //cout<<"place_taran_arc"<<place_taran_arc->Attribute("id")<<endl;
            //	cout << "second value: "<<place_taran_arc->Attribute("id") << endl;

        }


        // loopindex++;
        // cout<<"loopindex: "<<loopindex<<endl;

    } //end while /for
    return true;

}

const set<int> &NewNet::getMObsAttTrans() const {
    return mObsAttTrans;
}

const set<int> &NewNet::getMObsSupTrans() const {
    return mObsSupTrans;
}

const set<int> &NewNet::getMControllableTrans() const {
    return mControllableTrans;
}
//end bool function
