//
// Created by lipn on 19/01/2022.
//

#include "Hyper_SOG.h"
#include <iostream>     // std::cout
#include <algorithm>    // std::find
#include <vector>
#include <fstream>
#include <algorithm>
#include "opacity/OS_SuperAggregate.h"
#include "opacity/SimpleNode.h"
#include "opacity/SimpleGraph.h"
/*
 * Find whether a super aggregate exists or not
 */
OS_SuperAggregate *Hyper_SOG::find(OS_SuperAggregate *c) {

    for (const auto &agg: mGONodes) {
        if (*c == *agg) return agg;
    }
    return nullptr;
}


int Hyper_SOG::NbBddNode(OS_SuperAggregate *S, unsigned long &nb) {
    //cout << "appele de la methode NbBddNOde "<< endl;

    if (S->Visited == false) {
        //cout<<"insertion du meta etat numero :"<<nb<<"son id est :"<<S->mBddState.id()<<endl;
        //cout<<"sa taille est :"<<bdd_nodecount(S->mBddState)<<" noeuds \n";

        S->Visited = true;
        int bddnode = 0;
        //cout << " le bdd node count is "<< bddnode ;
        int size_succ = 0;
        for (auto i = S->beginSucc(); !(i == S->endSucc()); i++) {
            if ((*i).first->Visited == false) {
                nb++;
                size_succ += NbBddNode((*i).first, nb);
            }
        }
        return size_succ + bddnode;

    } else
        return 0;
}



void Hyper_SOG::insert(OS_SuperAggregate *c) {
    c->Visited = false;
    mGONodes.push_back(c);
}

void Hyper_SOG::deleteSA(OS_SuperAggregate *c) {
    // Check existance
    auto it_sa {std::find(mGONodes.begin(), mGONodes.end(),c)};
    if ( it_sa== mGONodes.end()) return;
    // Delete predecessors links
    auto lPred {c->getPredecessors()};
    for (auto & elt : lPred) {
        (elt.first)->deleteSuccTo(c);
    }
    //
    auto lSucc {c->getSuccesors()};
    for (auto & elt : lSucc) {
        (elt.first)->deletePredFrom(c);
    }
    mGONodes.erase(it_sa);
    //delete (c);
}

void Hyper_SOG::setInitialState(OS_SuperAggregate *c) { //Define the initial state of this graph
    mInitialSuperAggregate = c;
}

/*
 * Create a dot file graph
 * @param base_name the dot filename
 */
void Hyper_SOG::buildDOT(const std::string &base_name,std::set<edge_sa_t> &omega) {
    // std::cout << "Generate dot file: "<<mGONodes.size()<<" super-aggregates...\n";
    std::ofstream myfile;
    myfile.open(base_name + ".dot");
    myfile << "digraph " << "fichier " << "{" << endl;
    myfile << "compound=true" << endl;
    size_t index_hnode{0};
    for (const auto &hyper_node: mGONodes) {
        myfile << R"(subgraph "cluster)" << std::to_string(index_hnode) << R"(")" << " {" << endl;
        myfile << R"(label=")" << std::to_string(index_hnode) << R"("; )" << '\n';
        size_t index_agg{0};
        for (const auto &agg: hyper_node->m_lAggregates) {
            myfile <<"a"<<std::to_string(index_hnode)<<"_"<<std::to_string(index_agg) << ' ';
            myfile << R"( [label=")" << agg->mBddState << R"("];)" << '\n';
            ++index_agg;
        }
        myfile << "}\n";
        ++index_hnode;
    }

    for (size_t index_sa {0};index_sa<mGONodes.size();++index_sa) {
        for (size_t index_agg {0};index_agg<mGONodes[index_sa]->m_lAggregates.size(); ++index_agg) {
            for (const auto & edge : mGONodes[index_sa]->m_lAggregates[index_agg]->Successors) {
                myfile <<"a"<<std::to_string(index_sa)<<"_"<< std::to_string(index_agg) << ' ';
                myfile << " -> ";
                auto dest_sa {edge.first->m_parent};
                auto dest_index_sa {findPos(dest_sa)};
                auto dest_index_agg {dest_sa->findPos(edge.first)};
                myfile<<"a"<<dest_index_sa<<"_"<<dest_index_agg<<" ";
                myfile << "[label=\"" << mRdPBDD->mlTransitions[edge.second].name << "\"]" << " ;" << endl;
            }
        }
    }
    //Build set of non opaque SA
    set<OS_SuperAggregate*> l_non_opaque;
    for (const auto & elt : omega) {
        l_non_opaque.insert(elt.destination);
    }
    myfile<<R"(node [style=filled,color=".7 .3 1.0"];)"<<endl;
    index_hnode=mGONodes.size();
    for(const auto & elt : l_non_opaque) {
        myfile << R"(subgraph "cluster)" << std::to_string(index_hnode) << R"(")" << " {" << endl;
        myfile << R"(label=")" << std::to_string(index_hnode) << R"("; )" << '\n';
        size_t index_agg{0};
        for (const auto &agg: elt->m_lAggregates) {
            myfile <<"a"<<std::to_string(index_hnode)<<"_"<<std::to_string(index_agg) << ' ';
            myfile << R"( [label=")" << agg->mBddState << R"("];)" << '\n';
            ++index_agg;
        }
        myfile << "}\n";
        ++index_hnode;
    }
    // Disabled edges
    myfile<<"edge [color=red];\n";
    for (const auto &edge: omega) {
        auto it {std::find(mGONodes.begin(),mGONodes.end(),edge.source)};
        auto pos_source {std::distance(mGONodes.begin(),it)};
        myfile <<"a"<<std::to_string(pos_source)<<"_"<< 0 << ' ';
        myfile << " -> ";

        auto it_dest {l_non_opaque.find(edge.destination)};
        size_t pos_dest {std::distance(l_non_opaque.begin(),it_dest)+mGONodes.size()};
        myfile<<"a"<<pos_dest<<"_"<<0<<" ";
        myfile << "[label=\"" << mRdPBDD->mlTransitions[edge.transition].name << "\"]" << " ;" << endl;
    }

    myfile << "}" << endl;
    myfile.close();
}

size_t Hyper_SOG::findPos(OS_SuperAggregate *sa) {
    for(size_t i {0}; i <mGONodes.size();++i) {
        if (sa==mGONodes[i]) return i;
    }
    return -1;
}

Hyper_SOG::Hyper_SOG(const Hyper_SOG &other) {
    mRdPBDD=other.mRdPBDD;
    mInitialSuperAggregate=other.mInitialSuperAggregate;
    std::copy(other.mGONodes.begin(), other.mGONodes.end(),mGONodes.begin());
}

Hyper_SOG& Hyper_SOG::operator=(const Hyper_SOG & other) {
    if (&other==this) return *this;
    mRdPBDD=other.mRdPBDD;
    mInitialSuperAggregate=other.mInitialSuperAggregate;
    std::copy(other.mGONodes.begin(), other.mGONodes.end(),mGONodes.begin());
    return *this;
}

std::string Hyper_SOG::buildREX(OS_SuperAggregate* acceptance_sa) {
   return buildSupervisorGraph(acceptance_sa)->getREX();
}

std::shared_ptr<SimpleGraph> Hyper_SOG::buildSupervisorGraph(OS_SuperAggregate* acceptance_sa) {
    auto myGraph {std::make_shared<SimpleGraph>()};
    for (const auto & elt : mGONodes) {
        auto new_node {new SimpleNode()};
        if (elt==acceptance_sa) new_node->setAccept();
        myGraph->m_nodes.emplace_back(new_node);
    }
    // Insert links
    auto it_node {myGraph->m_nodes.begin()};
    for (const auto & elt : mGONodes) {
        //Succesors
        for (const auto & succ : elt->getSuccesors()) {
            auto pos {findPos(succ.first)};
            (*it_node)->m_successors.emplace_back(myGraph->m_nodes[pos],mRdPBDD->mlTransitions[succ.second].name);

        }
        // Predecessors
        for (const auto & pred : elt->getPredecessors()) {
            auto pos {findPos(pred.first)};
            (*it_node)->m_predecessors.emplace_back(myGraph->m_nodes[pos], mRdPBDD->mlTransitions[pred.second].name);

        }
        ++it_node;
    }
    return myGraph;
}


uint32_t Hyper_SOG::computeShortestPath() {
    if (mGONodes.size()==0) return 0;
    // Initialisation
    for (auto & sa : mGONodes) {
        for (auto & agg : sa->m_lAggregates) {
            agg->mDistance=INT32_MAX;
        }
    }
    mGONodes[0]->getInitialAggregate()->mDistance=0;

    // Start with selecting the aggregate with minimal distance
    Aggregate* it_min;
    int32_t distance_min {INT32_MAX};
    do {
        it_min= nullptr;
        distance_min=INT32_MAX;
        for (const auto &sa: mGONodes) {
            for (auto &agg: sa->m_lAggregates) {
                if (!agg->mVisited && agg->mDistance < distance_min) {
                    it_min = agg;
                    distance_min = agg->mDistance;
                }
            }
        }
        if (it_min) {
            // Update neighbours
            for (auto &succ: (*it_min).Successors) {
                succ.first->mDistance = std::min(succ.first->mDistance, it_min->mDistance + 1);
                if (succ.first->mDistance < distance_min) {
                    distance_min = succ.first->mDistance;
                    it_min = succ.first;
                }
            }
            (*it_min).mVisited = true;
        }
    } while (it_min!= nullptr);
    // Display distances
    for (auto & sa : mGONodes) {
        for (auto & agg : sa->m_lAggregates) {
            std::cout<<"Distance : "<<agg->mDistance<<std::endl;
        }
    }


    return 0;
}

size_t Hyper_SOG::getAggCount() const {
    size_t res {0};
    for (const auto &sa: mGONodes) {
        res+=sa->m_lAggregates.size();
    }
    return res;
}

std::shared_ptr<SimpleGraph> Hyper_SOG::buildExtendedSupervisorGraph(std::vector<edge_sa_t>& omega) {
    auto myGraph {std::make_shared<SimpleGraph>()};
    for (const auto & elt : mGONodes) {
        auto new_node {new SimpleNode()};
        //if (elt==acceptance_sa) new_node->setAccept();
        auto res = std::find_if(omega.begin(),omega.end(),[elt](edge_sa_t v){
            return elt==v.source;
        });
        if (res!=omega.end()) {
            new_node->setAccept();
            new_node->addOmegaElement(*res);
        }
        myGraph->m_nodes.emplace_back(new_node);
    }
    // Insert links
    auto it_node {myGraph->m_nodes.begin()};
    for (const auto & elt : mGONodes) {
        //Succesors
        for (const auto & succ : elt->getSuccesors()) {
            auto pos {findPos(succ.first)};
            (*it_node)->m_successors.emplace_back(myGraph->m_nodes[pos],mRdPBDD->mlTransitions[succ.second].name);

        }
        // Predecessors
        for (const auto & pred : elt->getPredecessors()) {
            auto pos {findPos(pred.first)};
            (*it_node)->m_predecessors.emplace_back(myGraph->m_nodes[pos], mRdPBDD->mlTransitions[pred.second].name);

        }
        ++it_node;
    }
    return myGraph;
}