# set minimum cmake version
cmake_minimum_required(VERSION 3.8 FATAL_ERROR)


 

# GOS executable
add_executable(gos main.cpp
  GOSConfig.h
        ../libraries/CLI11/CLI11.hpp
        NewNet.cpp
        NewNet.h
        opacity/OS_SuperAggregate.cpp opacity/OS_SuperAggregate.h

        Aggregate.h
        RdPBDD.cpp
        RdPBDD.h
        opacity/OS_Opacity_Manager.cpp
        opacity/OS_Opacity_Manager.h
        Hyper_SOG.h
        Hyper_SOG.cpp

        misc.h
        opacity/SimpleGraph.cpp
        opacity/SimpleGraph.h
        opacity/SimpleNode.cpp
        opacity/SimpleNode.h
        opacity/WeakSupervisor.cpp
        opacity/WeakSupervisor.h)


target_include_directories(gos PUBLIC  "${PROJECT_BINARY_DIR}/src")

target_link_libraries(gos
  RdP
        bdd
        tinyxml2
)
