//
// Created by lipn on 19/01/2022.
//

#ifndef GOS_HYPER_SOG_H
#define GOS_HYPER_SOG_H
#include <memory>
#include "opacity/OS_SuperAggregate.h"
#include "RdPBDD.h"
#include "opacity/SimpleGraph.h"

typedef pair<OS_SuperAggregate *, int> Hyper_Edge;
typedef vector<Hyper_Edge> hyper_edge_t;

class Hyper_SOG {
private:
    vector<OS_SuperAggregate *> mGONodes;
    RdPBDD *mRdPBDD;
public:
    OS_SuperAggregate *mInitialSuperAggregate;
    Hyper_SOG(RdPBDD *rdp) {
        mRdPBDD=rdp;
    }
    Hyper_SOG(const Hyper_SOG &);
    OS_SuperAggregate *find(OS_SuperAggregate *);
    hyper_edge_t &get_successor(OS_SuperAggregate *);
    int NbBddNode(OS_SuperAggregate *, unsigned long &);
    void insert(OS_SuperAggregate *);
    void deleteSA(OS_SuperAggregate *);
    void setInitialState(OS_SuperAggregate *);  //Define the initial state of this graph
    void buildDOT(const std::string &,std::set<edge_sa_t> &omega);
    size_t findPos(OS_SuperAggregate *);
    Hyper_SOG& operator=(const Hyper_SOG & other);
    std::string buildREX(OS_SuperAggregate* acceptance_sa);
    std::shared_ptr<SimpleGraph> buildSupervisorGraph(OS_SuperAggregate* acceptance_sa);
    std::shared_ptr<SimpleGraph> buildExtendedSupervisorGraph(std::vector<edge_sa_t>& omega);
    uint32_t computeShortestPath();
    size_t getAggCount() const;
};


#endif //GOS_HYPER_SOG_H
