//
// Created by chiheb on 31/12/23.
//

#include "SimpleNode.h"


bool SimpleNode::isAccept() const {
    return m_is_acceptance;
}

void SimpleNode::setAccept() {
    m_is_acceptance=true;
}

void SimpleNode::addOmegaElement(const edge_sa_t &e) {
    m_omega.emplace(e);
}

