//
// Created by chiheb on 31/12/23.
//

#include "SimpleGraph.h"
#include <algorithm>
#include <sstream>
#include <iostream>

using std::stringstream;

SimpleGraph::~SimpleGraph() {
    for (auto &elt: m_nodes)
        delete elt;
}

string SimpleGraph::getREX() {
    // Make uniform NFA
    if (m_nodes.size()==0) return "";
    if (!m_nodes[0]->m_predecessors.empty() || m_nodes[0]->isAccept()) {
        auto new_initial{new SimpleNode};
        new_initial->m_successors.emplace_back(m_nodes[0], "\u03B5");
        m_nodes[0]->m_predecessors.emplace_back(new_initial, "\u03B5");
        m_nodes.insert(m_nodes.begin(), new_initial);
    }

    auto it_accept{find_if(m_nodes.begin(), m_nodes.end(), [](const auto &node) {
        return node->isAccept();
    })};

    if (!(*it_accept)->m_successors.empty()) {
        auto new_final{new SimpleNode};
        new_final->m_predecessors.emplace_back((*it_accept), "");
        (*it_accept)->m_successors.emplace_back(new_final, "");
        m_nodes.emplace_back(new_final);
    }

    while (m_nodes.size() > 2) {
        auto it_node{m_nodes.begin() + 1};
        // Get reflexive
        stringstream ref;
        for (const auto &edge: (*it_node)->m_successors) {
            if (edge.first == *it_node) {
                ref << '(' << edge.second << ")*";
            }
        }
        // Remove reflexive edges
        /*(*it_node)->m_successors.erase(std::remove_if((*it_node)->m_successors.begin(), (*it_node)->m_successors.end(), [it_node](const std::pair<SimpleNode*,string> e) {
            return (e.first == (*it_node));
        }));
        (*it_node)->m_predecessors.erase(std::remove_if((*it_node)->m_predecessors.begin(), (*it_node)->m_predecessors.end(), [it_node](const std::pair<SimpleNode*,string> e) {
            return (e.first == (*it_node));
        }));*/
        auto it_succ {(*it_node)->m_successors.begin()};
        while (it_succ!=(*it_node)->m_successors.end()) {
            if ((*it_succ).first==*it_node) {
                (*it_node)->m_successors.erase(it_succ);
            }
            else ++it_succ;
        }
        auto it_pred {(*it_node)->m_predecessors.begin()};
        while (it_pred!=(*it_node)->m_predecessors.end()) {
            if ((*it_pred).first==*it_node) {
                (*it_node)->m_predecessors.erase(it_pred);
            }
            else ++it_pred;
        }


        // Update input/output edges
        for (const auto &in_edge: (*it_node)->m_predecessors) {
            for (const auto &out_edge: (*it_node)->m_successors) {
                stringstream transition;
                if (ref.str().length()>1 or out_edge.second.length()>1)
                    transition<<in_edge.second<<'.'<<ref.str()<<out_edge.second;
                else
                    transition<<in_edge.second<<ref.str()<<out_edge.second;
                addEdges(in_edge.first,out_edge.first,transition.str());
            }
        }
        removeNode(*it_node);
    }
    stringstream result;
    for (const auto &out_edge: m_nodes[0]->m_successors) {
        result<<out_edge.second<<" + ";
    }
    return result.str().substr(0,result.str().length()-3);
}

void SimpleGraph::removeNode(SimpleNode *node) {
    //Remove input edges
    for (const auto &in_edge: node->m_predecessors) {
        auto pred_node{in_edge.first};
        auto it {std::remove_if(pred_node->m_successors.begin(), pred_node->m_successors.end(), [node](const auto &elt) { return elt.first == node; })};
        pred_node->m_successors.erase(it,pred_node->m_successors.end());
    }
    // Remove output edges
    for (const auto &out_edge: node->m_successors) {
        auto succ_node {out_edge.first};
        auto it {std::remove_if(succ_node->m_predecessors.begin(),succ_node->m_predecessors.end(),[node](const auto &elt) { return elt.first ==node;})};
        succ_node->m_predecessors.erase(it,succ_node->m_predecessors.end());
    }
    //Remove node
    for (auto it {m_nodes.begin()};it<m_nodes.end();++it) {
        if (*it==node) {
            m_nodes.erase(it);
            break;
        }
    }

}

void SimpleGraph::addEdges(SimpleNode *source, SimpleNode *dest,const string& transition) {
    source->m_successors.emplace_back(dest,transition);
    dest->m_predecessors.emplace_back(source,transition);
}