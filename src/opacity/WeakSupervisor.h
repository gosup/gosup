//
// Created by chiheb on 17/05/24.
//

#ifndef GOS_WEAKSUPERVISOR_H
#define GOS_WEAKSUPERVISOR_H
#include "OS_Opacity_Manager.h"
#include <memory>
class WeakSupervisor {
    std::vector<std::shared_ptr<OS_Opacity_Manager>> mlOpacityManagers;
    std::shared_ptr<SimpleGraph> mWeakSupervisor {nullptr};
    void computeSynchronisedProduct();
public:
    WeakSupervisor()=default;
    void add(std::shared_ptr<OS_Opacity_Manager> manager);
    void comnputeSupervisor();
};


#endif //GOS_WEAKSUPERVISOR_H
