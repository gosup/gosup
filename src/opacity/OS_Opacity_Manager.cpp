﻿#include <algorithm>
#include "OS_Opacity_Manager.h"
#include "Hyper_SOG.h"
#include "bdd.h"
#include "opacity/SimpleGraph.h"

OS_Opacity_Manager::OS_Opacity_Manager(RdPBDD *model, const set<int> &obsAtt, const set<int> &obsSup,
                                       string name) : mHyperSOG(model) {
    m_name = name;
    this->m_model = model;
    mObsAttack = obsAtt;

    for (const auto &elt: obsSup) {
        if (find(mObsAttack.begin(), mObsAttack.end(), elt) == mObsAttack.end())
            mObsSup.insert(elt);
    }
}


bool
OS_Opacity_Manager::opacifyByControl_Hyper_SOG(std::set<OS_SuperAggregate *> &toBeRemoved,
                                               supervision_trace_t &supervision, element_t &elt, int t,
                                               OS_SuperAggregate *superAgg) {
    if (superAgg->isIncludedIn(m_model->mSecretBdd)) {
        std::cout << "Super is in secret...\n";
        toBeRemoved.insert(superAgg);
    } else {
        std::cout << "Trim...\n";
        trim(superAgg);
    }

    supervision.base_trace = elt.trace;
    if (m_model->mControllable.find(t) != m_model->mControllable.end()) {
        supervision.base_trace.pop_back();
        local_trace_t el{.disabled_transition = t};
        supervision.local_traces.emplace_back(el);
        return true;
    } else {
        while (!supervision.base_trace.empty()) {
            auto transition = supervision.base_trace.back();
            supervision.base_trace.pop_back();
            if (m_model->mControllable.find(transition) != m_model->mControllable.end()) {
                local_trace_t el{.disabled_transition = transition};
                return true;
            }
        }
        return false;
    }
}


/*
 * @Desc Compute the Hyper-SOG and opacify on the fly
 */
void OS_Opacity_Manager::computeAndOpacify_Hyper_SOG() {
    //Construction and opacification
    /*cout << "Compute Hyper-SOG and opacify..." << endl;

    cout << "Attacker observable transitions: ";
    for (const auto &elt: mObsAttack) cout << m_model->mlTransitions[elt].name << ' ';
    cout << endl;

    cout << "Supervisor observable transitions: ";
    for (const auto &elt: mObsSup) cout << m_model->mlTransitions[elt].name << ' ';
    cout << endl;

    cout << "Non observable transitions: ";
    for (const auto &elt: m_model->mNonObservables) cout << elt << ' ';
    cout << endl;

    cout << "Controllable transitions: ";
    for (const auto &elt: m_model->mControllable) cout << elt << ' ';
    cout << endl;

    // Saturate(M0): initial state
    cout << "Initial: " << m_model->mInitialMarking << endl;*/
    bdd Complete_meta_state = this->m_model->Accessible_epsilon(m_model->mInitialMarking);

    //get the corresponding super aggregate
    OS_SuperAggregate *initialSupAgg = m_model->computeSuperAggregate(Complete_meta_state, mObsSup);

    mHyperSOG.setInitialState(initialSupAgg);
    mHyperSOG.insert(initialSupAgg);
    if (m_model->mSecretBdd == bdd_false()) {
        cout << "No specified secret states: nothing to enforce!" << endl;
        return;
    }
    if (initialSupAgg->isIncludedIn(m_model->mSecretBdd)) {
        cout << "Opacity can not be enforced by control, initial super aggregate is not opaque" << endl;
        return;
    }
    // transitions enabled by this initial Super Aggregate
    setPairClassSetint fireAttack = m_model->firableObs(*initialSupAgg, mObsAttack);

    list<int> trace;
    myStack_t myStack;
    Set fire;
    element_t elt_stack{.sourceSupAgg = initialSupAgg, .sourceAgg_trans = fireAttack, .trace = trace};
    myStack.push(elt_stack);
    std::set<OS_SuperAggregate *> lSecretSA, lPartialSecretSA;
    while (!myStack.empty()) {
        elt_stack = myStack.top();
        auto setFirings = elt_stack.sourceAgg_trans;
        trace = elt_stack.trace;
        myStack.pop();
        for (const auto &e: setFirings) {
            auto sourceAggregate = e.first;
            auto lTransitions = e.second;
            bdd succ, completeSucc;
            Aggregate *destAggregate = nullptr;
            for (const auto &transition: lTransitions) {
                trace.push_back(transition);
                succ = m_model->get_successor(sourceAggregate->mBddState, transition);
                completeSucc = m_model->Accessible_epsilon(succ);

                OS_SuperAggregate *destSuperAggregate = m_model->computeSuperAggregate(completeSucc, mObsSup);
                OS_SuperAggregate *existantSuperAggregate = mHyperSOG.find(destSuperAggregate);
                if (existantSuperAggregate == nullptr) {
                    mHyperSOG.insert(destSuperAggregate);
                    if (destSuperAggregate->isIncludedIn(m_model->mSecretBdd)) {
                        lSecretSA.insert(destSuperAggregate);
                    } else {
                        fireAttack = m_model->firableObs(*destSuperAggregate, mObsAttack);
                        elt_stack = {destSuperAggregate, fireAttack, trace};
                        myStack.push(elt_stack);
                        if (destSuperAggregate->containBDD(m_model->mSecretBdd)) {
                            lPartialSecretSA.insert(destSuperAggregate);
                        }
                    }
                } else {
                    delete destSuperAggregate;
                    destSuperAggregate = existantSuperAggregate;
                }

                destAggregate = destSuperAggregate->find(completeSucc);
                // Insert edges
                sourceAggregate->Successors.emplace_back(destAggregate, transition);
                destAggregate->Predecessors.emplace_back(sourceAggregate, transition);
            }
        }
    }
    generateDOT("_");
    opacify(lSecretSA, lPartialSecretSA);
    generateDOT();
    //printSequences();
    if (m_isOpaque)
        std::cout << "The system is already opaque.. No computed supervisor...\n";
    else
    if (m_omega.empty())
        std::cout << "Can't compute supervisor\n";
    for (const auto &elt: m_omega) {
        std::cout << "\n===>Control in (" << elt.source->getInitialAggregate()->mBddState << "," << m_model->
                mlTransitions[elt.transition].name << ")\n";
        std::cout << "  => " << mHyperSOG.buildREX(elt.source) << "\n";
    }
} //end fct

OS_SuperAggregate *OS_Opacity_Manager::trim(OS_SuperAggregate *original) {
    auto secret{m_model->mSecretBdd};
    auto moveSecretFrom = [secret](OS_SuperAggregate *original, OS_SuperAggregate *new_super_agg, bool keep) {
        auto it_source{original->m_lAggregates.begin()};

        auto condition = [](const bdd &elt, const bdd &secret, bool keep) {
            return keep ? (elt & secret) != bdd_false() : (elt & secret) == bdd_false();
        };

        while (it_source != original->m_lAggregates.end()) {
            auto current_agg{*it_source};
            if (condition(current_agg->mBddState, secret, keep)) {
                new_super_agg->insert(current_agg);
                original->m_lAggregates.erase(it_source);
            } else ++it_source;
        }
    };

    auto new_super_agg{new OS_SuperAggregate};
    if ((original->getInitialAggregate()->mBddState & secret) == bdd_false()) {
        moveSecretFrom(original, new_super_agg, true);
        return new_super_agg;
    } else {
        moveSecretFrom(original, new_super_agg, false);
        return original;
    }
}


void OS_Opacity_Manager::generateDOT(const string &&suffixe) {
    mHyperSOG.buildDOT(suffixe + m_name, m_omega);
}

void OS_Opacity_Manager::opacify(std::set<OS_SuperAggregate *> &lSecretSA,
                                 std::set<OS_SuperAggregate *> &lPartialSecSA) {
    if (lSecretSA.empty() ) {m_isOpaque=true;}
    // Step 1 : trim partial secret super-aggregates
    for (auto &elt: lPartialSecSA) {
        trim(elt);
    }

    // Step 2 : remove secret super-aggregates
    while (!lSecretSA.empty()) {
        auto it{lSecretSA.begin()};
        auto lPred{(*it)->getPredecessors()};
        auto it_edge{lPred.begin()};
        while (it_edge != lPred.end()) {
            auto it_isControllable{
                find(m_model->mControllable.begin(), m_model->mControllable.end(), (*it_edge).second)
            };
            auto it_isSecretSA{lSecretSA.find((*it_edge).first)};

            if (it_isControllable != m_model->mControllable.end()) {
                if (it_isSecretSA == lSecretSA.end()) {
                    m_omega.insert({(*it_edge).first, (*it_edge).second, *it});
                }
            } else {
                lSecretSA.insert((*it_edge).first);
            }
            ++it_edge;
        }
        mHyperSOG.deleteSA(*it);
        lSecretSA.erase(it);
    }
}

void OS_Opacity_Manager::printSequences() {
    std::cout << "#Omega: " << m_omega.size() << '\n';
    for (const auto &elt: m_omega) {
        std::cout << "\nControl in (" << elt.source->getInitialAggregate()->mBddState << "," << m_model->mlTransitions[
            elt.transition].name << ")\n";
        std::stack<sequence_t> my_stack;
        sequence_t sequence{
            .current_sa = mHyperSOG.mInitialSuperAggregate,
            .list_succ = mHyperSOG.mInitialSuperAggregate->getSuccesors()
        };
        my_stack.push(sequence);
        printElementOmega(elt, my_stack);
    }
}

void OS_Opacity_Manager::printElementOmega(const edge_sa_t &edge, std::stack<sequence_t> &_stack) {
    if (_stack.empty()) return;
    sequence_t sequence{_stack.top()};
    _stack.pop();
    // Identify all cycles
    auto it_next{sequence.list_succ.begin()};
    while (it_next != sequence.list_succ.end()) {
        auto next{it_next->first};
        auto predicate = [next](element_sequence_t &e) {
            return e.source_sa == next;
        };
        auto it{find_if(sequence.trace.begin(), sequence.trace.end(), predicate)};
        if (it != sequence.trace.end()) {
            // Cycle detected
            //std::cout << "cycle\n";
            vector<int> cycle;
            for (auto it_index{it + 1}; it_index != sequence.trace.end(); ++it_index) {
                cycle.emplace_back((*it_index).transition);
            }
            cycle.emplace_back((*it_next).second);
            it->lCycles.insert(cycle);
            sequence.list_succ.erase(it_next);
            it_next = sequence.list_succ.begin();
        } else {
            ++it_next;
        }
    }
    //
    if (sequence.current_sa == edge.source) {
        std::cout << "==> \u03B5";
        for (const auto &e: sequence.trace) {
            std::cout << m_model->mlTransitions[e.transition].name;
            for (const auto &cycle: e.lCycles) {
                std::cout << '[';
                for (const auto &t: cycle) {
                    std::cout << m_model->mlTransitions[t].name;
                }
                std::cout << "]*";
            }
        }
        std::cout << " ; transition to be disabled: " << m_model->mlTransitions[edge.transition].name << '\n';
        printElementOmega(edge, _stack);
        return;
    }
    // No cycle
    if (!sequence.list_succ.empty()) {
        auto it_successor{sequence.list_succ.begin()};
        sequence_t successor{
            .current_sa = (*it_successor).first, .list_succ = (*it_successor).first->getSuccesors(),
            .trace = sequence.trace
        };
        element_sequence_t elt_trace;
        elt_trace.source_sa = (*it_successor).first; //sequence.current_sa->getSuccesors()[sequence.current_succ].first;
        elt_trace.transition = (*it_successor).second;
        //sequence.current_sa->getSuccesors()[sequence.current_succ].second;
        successor.trace.push_back(elt_trace);
        sequence.list_succ.erase(it_successor);
        _stack.push(sequence);
        _stack.push(successor);
    }
    printElementOmega(edge, _stack);
}


std::size_t OS_Opacity_Manager::getAggCount() {
    return mHyperSOG.getAggCount();
}

bool OS_Opacity_Manager::isOpaque() const {
    return m_isOpaque;
}
