//
// Created by chiheb on 03/03/2022.
//
#include <algorithm>
#include "OS_SuperAggregate.h"

/*
 * @Desc : check whether a set of states is included in a super aggregate
 * @param : set of states
 */
bool OS_SuperAggregate::isIncludedIn(const bdd &states) {

    for ( const auto &elt: m_lAggregates) {
        if ((elt->mBddState & states)!= elt->mBddState)  return false;
    }
    return true;


    return !std::any_of(m_lAggregates.begin(),m_lAggregates.end(),[states](const auto & elt) {
        return (elt->mBddState & states)== bdd_false();
    });
}

/*
 * @Desc : check whether a Super-aggregate contains some secret states
 * @param : set of bdd states
 */
bool OS_SuperAggregate::containBDD(const bdd & states) {
    return std::any_of(m_lAggregates.begin(),m_lAggregates.end(),[states](const auto & elt) {
        return (elt->mBddState & states)!= bdd_false();
    });
}

OS_SuperAggregate::~OS_SuperAggregate() {
    for (const auto &elt: m_lAggregates) {
        delete elt;
    }
}

bool OS_SuperAggregate::operator==(const OS_SuperAggregate &supAgg) const {
    bool res = true;
    for (const auto &elt: m_lAggregates) {
        if (!supAgg.find(elt->mBddState)) return false;
    }
    return true;
}

Aggregate *OS_SuperAggregate::find(const bdd &b) {
    for (const auto &elt: m_lAggregates) {
        if (elt->mBddState == b) return elt;
    }

    return nullptr;
}

size_t OS_SuperAggregate::findPos(Aggregate *agg) {
    for (size_t i{0}; i < m_lAggregates.size(); ++i) {
        if (agg == m_lAggregates[i]) return i;
    }
    return -1;
}

void OS_SuperAggregate::setInitialAggregate(Aggregate *c) {
    m_initialstate = c;
}

Aggregate *OS_SuperAggregate::getInitialAggregate() {
    return m_initialstate;
}


void OS_SuperAggregate::insert(Aggregate *c) {
    c->m_parent = this;
    m_lAggregates.emplace_back(c);
}

void OS_SuperAggregate::deleteSuccTo(OS_SuperAggregate *dest) {
    for (auto & agg : m_lAggregates) {
        agg->Successors.erase(std::remove_if(agg->Successors.begin(),agg->Successors.end(),[dest](std::pair<Aggregate*,int>& elt) {
            return (dest==elt.first->m_parent);
        }),agg->Successors.end());
    }
}

void OS_SuperAggregate::deletePredFrom(OS_SuperAggregate *source) {
    for (auto & agg : m_lAggregates) {
        agg->Predecessors.erase(std::remove_if(agg->Predecessors.begin(),agg->Predecessors.end(),[source](std::pair<Aggregate*,int>& elt) {
            return (source==elt.first->m_parent);
        }),agg->Predecessors.end());
    }
}