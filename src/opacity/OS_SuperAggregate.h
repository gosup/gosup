//
// Created by chiheb on 03/03/2022.
//

#ifndef GOS_OS_SUPERAGGREGATE_H
#define GOS_OS_SUPERAGGREGATE_H

#include <unordered_set>
#include"../Aggregate.h"


struct OS_SuperAggregate {
    struct IteratorSucc {
        explicit IteratorSucc(OS_SuperAggregate * ptr):m_ptr(ptr){
        }

        std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator begin() {
            update();
            return m_succ.begin();
        }
        std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator end() { return m_succ.end(); }
        std::vector<std::pair<OS_SuperAggregate * ,int>>& getSuccessors() {
            update();
            return m_succ;
        }
    private:
        void update() {
            m_succ.clear();
            for (const auto & agg : m_ptr->m_lAggregates) {
                for (const auto & succ : agg->Successors) {
                    if (succ.first->m_parent!=m_ptr) {
                        m_succ.emplace_back(succ.first->m_parent, succ.second);
                    }
                }
            }
        }
        OS_SuperAggregate *m_ptr;
        std::vector<std::pair<OS_SuperAggregate * ,int>> m_succ;

    };

    struct IteratorPred {
        IteratorPred(OS_SuperAggregate * ptr):m_ptr(ptr){

        }

        std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator begin() {
            update();
            return m_pred.begin();
        }
        std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator end() { return m_pred.end(); }
        std::vector<std::pair<OS_SuperAggregate * ,int>>& getPredecessors() {
            update();
            return m_pred;
        }
    private:
        void update() {
            m_pred.clear();
            for (const auto & agg : m_ptr->m_lAggregates) {
                for (const auto & pred : agg->Predecessors) {
                    if (pred.first->m_parent!=m_ptr) {
                        m_pred.emplace_back(pred.first->m_parent, pred.second);
                    }
                }
            }
        }
        OS_SuperAggregate *m_ptr;
        std::vector<std::pair<OS_SuperAggregate * ,int>> m_pred;
    };

    std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator beginSucc() {
        return m_iterator_succ.begin();
    }
    std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator endSucc() {
        return m_iterator_succ.end();
    }
    std::vector<std::pair<OS_SuperAggregate * ,int>>& getSuccesors(){
        return m_iterator_succ.getSuccessors();
    }

    std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator beginPred() {
        return m_iterator_pred.begin();
    }
    std::vector<std::pair<OS_SuperAggregate * ,int>>::iterator endPred() {
        return m_iterator_pred.end();
    }
    std::vector<std::pair<OS_SuperAggregate * ,int>>& getPredecessors(){
        return m_iterator_pred.getPredecessors();
    }

    bool operator==(const OS_SuperAggregate &) const;
    ~OS_SuperAggregate();
    bool isIncludedIn(const bdd &);
    bool containBDD(const bdd &);
    Aggregate *find(const bdd &);
    size_t findPos(Aggregate *);
    void setInitialAggregate(Aggregate *);
    Aggregate *getInitialAggregate();
    void insert(Aggregate *);
    void deleteSuccTo(OS_SuperAggregate *);
    void deletePredFrom(OS_SuperAggregate *);
    // Attributes
    vector<Aggregate*> m_lAggregates;
    bool Visited;
private:
    Aggregate *m_initialstate;
    IteratorSucc m_iterator_succ {this};
    IteratorPred m_iterator_pred {this};
};


#endif //GOS_OS_SUPERAGGREGATE_H
