//
// Created by chiheb on 31/12/23.
//

#ifndef GOS_SIMPLENODE_H
#define GOS_SIMPLENODE_H
#include <string>
#include <vector>
#include "misc.h"

using std::vector,std::string,std::pair;

class SimpleNode {
private:
    bool m_is_acceptance {false};
    std::set<edge_sa_t> m_omega;
public:
    bool isAccept() const;
    void setAccept();
    vector<std::pair<SimpleNode*,string>> m_predecessors, m_successors;
    void addOmegaElement(const edge_sa_t &e);
};


#endif //GOS_SIMPLENODE_HOU+