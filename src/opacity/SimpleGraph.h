//
// Created by chiheb on 31/12/23.
//

#ifndef GOS_SIMPLEGRAPH_H
#define GOS_SIMPLEGRAPH_H
#include <vector>
#include "SimpleNode.h"
using std::string;
class SimpleGraph {
public:
    std::vector<SimpleNode *> m_nodes;
    ~SimpleGraph();
    string getREX();
    void removeNode(SimpleNode *);
    static void addEdges(SimpleNode *,SimpleNode*,const string&);
};


#endif //GOS_SIMPLEGRAPH_H
