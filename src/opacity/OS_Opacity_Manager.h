/**
 * Implementation of an Opacity_Manager
 * A class that manages opacity of a model
 * Creates a supervisor @see OS_Supervisory_Manager
 * contains implementation of  OpacifyByControl(), CutOldBranches() and SlimOldBranches()
 *
 *
 */

#ifndef OS_OPACITY_MANAGER_H
#define OS_OPACITY_MANAGER_H
#include <iostream>
#include <vector>
#include <stack>
#include <set>
#include <algorithm>
#include "../RdPBDD.h"
#include "NewNet.h"
#include "Hyper_SOG.h"





struct element_sequence_t {
    OS_SuperAggregate *source_sa;
    int transition;
    set<vector<int>> lCycles;
    set<element_sequence_t*,int> lSucc;
};


struct sequence_t {
    OS_SuperAggregate* current_sa;
    std::vector<std::pair<OS_SuperAggregate*,int>> list_succ;
    vector<element_sequence_t> trace;
};
class OS_Opacity_Manager {
private:
    set<int> mObsAttack, mObsSup;
    std::string mTrace{""};
    OS_SuperAggregate* trim(OS_SuperAggregate *);
    void opacify(std::set<OS_SuperAggregate*>&,std::set<OS_SuperAggregate*>&);
    void printSequences();
    void printElementOmega(const edge_sa_t & edge,std::stack<sequence_t>& _stack);
    string m_name;
    RdPBDD *m_model;
    Hyper_SOG mHyperSOG;
    std::set<edge_sa_t> m_omega;
    bool m_isOpaque {false};
public:
    virtual ~OS_Opacity_Manager()=default;
    OS_Opacity_Manager(const OS_Opacity_Manager &) = delete;
    OS_Opacity_Manager(RdPBDD *model, const set<int> &obsAtt, const set<int> &obsSup, string name = "undefined");
    bool opacifyByControl_Hyper_SOG(std::set<OS_SuperAggregate *> &, supervision_trace_t &, element_t& elt,  int t, OS_SuperAggregate *superAgg);

    void computeAndOpacify_Hyper_SOG();

    /*Flow operators overload*/
    friend ostream &operator<<(ostream &flux, const OS_Opacity_Manager &manager);

    void generateDOT(const string &&suffixe= "");

    std::size_t getAggCount();


    friend class WeakSupervisor;
    bool isOpaque() const;
};

#endif // OS_OPACITY MANAGER_H
