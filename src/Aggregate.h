#ifndef CLASS_OF_STATE
#define CLASS_OF_STATE
using namespace std;

#include<unordered_map>
#include <set>
#include <vector>
#include "bdd.h"


typedef set<int> Set;
class OS_SuperAggregate;
struct Aggregate {
        Aggregate(OS_SuperAggregate *sup_agg):m_parent(sup_agg) {}
		Set firable;
		bdd mBddState;
        vector<pair<Aggregate*,int>> Predecessors, Successors;
        OS_SuperAggregate * m_parent;
        int32_t mDistance;
        bool mVisited {false};
};

#endif
