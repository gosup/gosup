//
// Created by chiheb on 13/03/2022.
//

#ifndef GOS_MISC_H
#define GOS_MISC_H
#include <set>
#include <list>
#include <stack>
#include "bdd.h"
#include "Aggregate.h"
#include "opacity/OS_SuperAggregate.h"

typedef std::set<std::pair<Aggregate*,std::set<int>>> setPairClassSetint;
struct element_t {
    OS_SuperAggregate* sourceSupAgg;
    setPairClassSetint sourceAgg_trans;
    list<int> trace;
};
using myStack_t=std::stack<element_t>;

struct local_trace_t {
    list<int> suffixe_trace;
    int disabled_transition;
};
struct supervision_trace_t {
    list<int> base_trace;
    list<local_trace_t> local_traces;
};


struct edge_sa_t {
    OS_SuperAggregate* source;
    int transition;;
    OS_SuperAggregate*destination;
    bool operator<(const edge_sa_t & other) const {
        return (source<other.source || destination<other.destination || transition<other.transition);
    }
};

//typedef std::stack<std::tuple<OS_SuperAggregate*,setPairClassSetint,vector<int>>> myStack_t;





#endif //GOS_MISC_H
