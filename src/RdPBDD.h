/* -*- C++ -*- */
#ifndef RdPBDD_H
#define RdPBDD_H

#include <vector>
#include <stack>
#include <unordered_set>
#include <utility>
#include <stdio.h>
#include <iostream>
#include "NewNet.h"
#include "bdd.h"



#include "opacity/OS_SuperAggregate.h"
#include "misc.h"

using namespace std;
/***********************************************/

typedef pair<Aggregate *, bdd> couple;
typedef pair<couple, Set> Pair;
typedef stack<Pair> pile;

typedef pair<OS_SuperAggregate *, bdd> couple_H;
typedef pair<couple_H, Set> Pair_H;
typedef stack<Pair_H> pile_H;

std::string IntToString(int number);

class Trans {
public:
    Trans();

    Trans(const bdd &var, bddPair *pair, const bdd &rel, const bdd &prerel, const bdd &Precond, const bdd &Postcond);

    Trans(const Trans &);//added by Nour
    Trans &operator=(Trans &);//added by Nour
    bdd operator()(const bdd &op) const;//Franchissement avant
    bdd operator[](const bdd &op) const;//Franchissement arrière
    friend bool operator<(Trans const &, Trans const &);//added by Nour

    bdd getRel();

    bdd getVar();

    void setRel(bdd);

    void setVar(bdd);

    friend class RdPBDD;

private:
    bdd var;
    bddPair *pair;
    bdd rel;
    bdd prerel;

    bdd Precond;
    bdd Postcond;


};

class RdPBDD  {
private :
    //vector<class Transition> transitions;
    //Set mObservable;

    vector<Set> mlSecretStates;
    Set InterfaceTrans;
    Set Formula_Trans;
    unsigned int Nb_places;
    map<string, int> mTransitionName;
    map<string, int> mPlaceName;
public:

    const NewNet *mPNet;
    vector<Transition> mlTransitions;
    set<int>  mObservables, mNonObservables,mControllable;
    bdd mInitialMarking;
    bdd mSecretBdd;
    bdd mCross;

    bdd Secretb;

    bdd T;
    bdd currentvar;
    vector<Trans> relation;
    vector<Place> places;


    bdd VerifiParent(bdd s, int f);
    bool FindSbVector(vector<string> a, string c);

    bdd GetPrerel(bdd s, int t);

    string GetFather(bdd d);

    /* G�n�ration de graphes d'observation */

    bdd Accessible_epsilon(bdd From);

    bdd Accessible_epsilon_parametrized(bdd From, set<int> NonObs);

    //quantified opacity Saturate function
    bdd QSaturate(bdd From, bool init);


    bdd StepForward(bdd From);

    // bdd get_Precedor(bdd From,int t);
    bdd StepBackward3(bdd From, int i);

    bdd VerificationPost(int t);




    bdd StepForward2(bdd From);

    bdd StepBackward(bdd From);

    //Set Set_Marked(bdd S);
    string ConvertToString(bdd S);

    bdd StepBackward2(bdd From);

    std::vector<string> Tokenizer(string s);

    bdd EmersonLey(bdd S, bool trace);



    Set firable_obs(bdd State);
    setPairClassSetint firableObs(const OS_SuperAggregate &,const Set&);
    bdd get_successor(bdd From, int t);


    bool SimpleOpacityVerifer(Set A, Set B, bdd c);

    bdd FrontiereNodes(bdd From) const;

    bdd CanonizeR(bdd s, unsigned int i);

    RdPBDD(const NewNet &, int BOUND = 32, bool init = false);

    ~RdPBDD() {};
    OS_SuperAggregate* computeSuperAggregate(const bdd &initial,const set<int> & obsTransitions);
};

/*____________Structure utiles aux produit synchronis� g�n�ralis� de n graphes d'observations ________*/

typedef pair<OS_SuperAggregate *, bdd *> Couple_H;

#endif
