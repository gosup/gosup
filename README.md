# General Framework Opacity Supervision 

# GoSup

(G)eneral Framework for (O)pacity (Sup)ervision using the (S)ymbolic (O)bservation (G)raph, a C/C++ tool that allows to enhance opacity of a model.

# Description

This repository hosts the experiments and results for our general approach to supervise the opacity of Discrete event Systems (DES). 
We develop a new version of the SOG and is called Hyper Symbolic Observation Graph (H-SOG for short).

# Dependencies

- [Cmake](https://cmake.org/)

## Project C++ Version and Compiler Requirements

This project utilizes C++20 features. It requires at least GCC version 11 or higher to compile successfully. To ensure compatibility, please make sure you have GCC 11 or higher installed on your system. You can update GCC on Ubuntu 18.04 by adding the Ubuntu Toolchain PPA with the following command:

```bash
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install gcc-11 g++-11
```

# Building
```bash
git clone --recursive https://depot.lipn.univ-paris13.fr/gosup/gosup.git

cd gosup && mkdir build && cd build

cmake ..

cmake --build .
```

## Running the Project

Before executing the `gos` executable, navigate to the `src` directory using the following command:

```bash
cd src
```

# Usage

```
./gos [OPTIONS]

Options:
  -h,--help                   Print this help message and exit
  --model Path:FILE ... REQUIRED
                              XML model files; set several files to perform modular supervision
  --obs Path:FILE REQUIRED    File specifying observable transitions resp. for supervisor and attacker, and controllable transitions
  --sec Path:FILE REQUIRED    File specifying secret states
```



# Publications

- A Novel Approach for Supervisor Synthesis to Enforce Opacity of Discrete Event Systems. ICICS 2021
- A General Framework for Supervision of Opacity, ICSOC 2021 (waiting for publication)
- Hyper Symbolic Observation Graph to Enforce Opacity of Discrete Event Systems using Supervisory Control CoDIt 2022 
- At Design-Time Approach for Supervisory
Control of Opacity, Coopis 2022
- Optimal Supervisory Control of Opacity for Modular Systems, International Symposium on Parallel and Distributed Processing and Applications ISPA/BDCloud/So-
cialCom/SustainCom 2022, Melbourne, Australia, December 17-19, 2022.
- Enforcing the Opacity of Modular Discrete Event Systems using Supervisory Control, 9th Interna-
tional Conference on Control, Decision and Information Technologies, CoDIT’23, 03-06
July, 2023, Rome, Italy.
